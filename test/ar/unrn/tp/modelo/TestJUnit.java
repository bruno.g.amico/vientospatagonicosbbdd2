package ar.unrn.tp.modelo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;

public class TestJUnit {

	/*
	 * CASO B.2 -CASO B.1 - Calcular el monto total del carrito. 
	 * Sin descuentos vigentes, pero con descuentos que ya caducaron
	 */
	@Test
	public void casoB1() throws Exception{

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20000;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 7, 1);
		LocalDate hasta = LocalDate.of(2022, 7, 31);	
		
		LocalDate desde2 = LocalDate.of(2022, 6, 1);
		LocalDate hasta2 = LocalDate.of(2022, 6, 15);
		
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		//NO VIGENTE  8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.08f, "VISA");		
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);					
		
		// Carrito, no hay promociones vigentes	
		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");// paso una tarjeta x
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjetaCliente);				
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.1 - Calcular el monto total del carrito con un descuento vigente para los\r\n" + 
				"productos marca ADIDAS");		
		float totalCarrito = carrito.calcularMontoTotal();
		System.out.println("Monto total carrito: " + totalCarrito);
		//NO hay descuentos vigentes, total 30000
		assertEquals(totalCarrito, 30000.0, 0f);
		System.out.println("***************************");
	}
			
	
	/*
	 * CASO B.2 - Calcular el monto total del carrito con un descuento vigente para los 
		productos marca ADIDAS
	 */
	@Test
	public void casoB2() throws Exception{

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20000;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 9, 15);		
		
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
			
		// Carrito, no hay promociones vigentes	
		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");// paso una tarjeta x
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjetaCliente);				
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.2 - Calcular el monto total del carrito con un descuento vigente para los\r\n" + 
				"productos marca ADIDAS");		
		float totalCarrito = carrito.calcularMontoTotal();
		System.out.println("Monto total carrito: " + totalCarrito);
		//5% de descuento en las zapas ADIDAS que cuestan 10000
		assertEquals(totalCarrito, 29500.0, 0f);
		System.out.println("***************************");
	}
		
	/*
	 * CASO B.3 - Calcular el monto total del carrito con un descuento vigente del tipo de Medio de pago 
	 */
	@Test 
	public void casoB3() throws Exception {

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20000;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 10);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 9, 10);			
		
		//marca adiddas promo
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//10% del 1/8 al 31/8, marca VISA - 8% (10% PRUEBO) de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, "VISA");		
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes	
		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");// paso una tarjeta x
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjetaCliente);		
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		carrito.listarProductos();
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.3 - Calcular el monto total del carrito con un descuento vigente del tipo\r\n" + 
				"de Medio de pago.");		
		float totalCarrito = carrito.calcularMontoTotal();
		System.out.println("Monto total carrito: " + totalCarrito);
		//10% en pago con visa de un total de 30000
		assertEquals(totalCarrito, 27000.0, 0f);
		System.out.println("***************************");			
	}	
	/*
	 * CASO B.4 - Calcular el monto total del carrito con dos descuentos vigentes, 
	 * sobre productos marca ADIDAS y para tarjeta de cr�dito VISA
	 * */
	@Test 
	public void casoB4() throws Exception {
		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20000;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 10000, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 9, 10);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 9, 10);			
		
		//marca adiddas promo
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//10% del 1/8 al 31/8, marca VISA - 8% (10% PRUEBO) de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, "VISA");
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes
		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");// paso una tarjeta x
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjetaCliente);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		//carrito.listarProductos();
		
		System.out.println("***************************");
		System.out.println("CASO B.4 - Calcular el monto total del carrito con dos descuentos vigentes, sobre productos marca ADIDAS y para tarjeta de cr�dito VISA");		
		float totalCarrito = carrito.calcularMontoTotal();
		System.out.println("Monto total carrito: " + totalCarrito);
		assertEquals(totalCarrito, 26550.0, 0f);
		System.out.println("***************************");			
	}	
	/*
	 * CASO B.5 - Realizar el pago y verificar que se genere la venta u orden de pago
	 * */
	@Test 
	public void casoB5() throws Exception {

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20000;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 9, 10);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 9, 10);			
		
		//marca adiddas promo
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//10% del 1/8 al 31/8, marca VISA - 8% (10% PRUEBO) de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, "VISA");
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes
		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");
		Tarjeta tarjetaCliente2 = new Tarjeta(100L, "1111222233335555", 555, 2018, 2023, "MASTER");	
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjetaCliente);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.5 - Realizar el pago y verificar que se genere la venta u orden de pago");
		Cliente cliente = new Cliente(1L, "Bruno Gabriel", "AMICO", "27828706", "bruno.g.amico@gmail.com");

		cliente.agregarTarjeta(tarjetaCliente);
		cliente.agregarTarjeta(tarjetaCliente2);
		
		//SOLO para test, al realizar la venta indico que tarjeta se va a usar... 
		//tener en cuenta que podria obtener la tarja directamente consultandole al cliente
		Venta venta = carrito.realizarVenta(cliente, cliente.dameTarjeta("1111222233334444"));
		System.out.println("Monto total carrito: " + venta.imprimir());
		assertEquals(venta.getMonto(), 26550.0, 0f);
		System.out.println("***************************");			
	}		
	
	/*
	 * CASO B.6 
	 * Verificar que no sea posible crear un Producto sin 
	 * categor�a,
	 * descripci�n 
	 * y precio.
	 * */
	@Test(expected = Exception.class)
	public void productoSinCategoria() throws Exception {
		String codigo = "9999";
		String desripcion = "Zapas le cop sportifens";
		float precio = 10000;
		Marca marca = new Marca("Le cop sportifens");		
		Producto producto = new Producto(codigo, desripcion, precio, null, marca);
	}

	@Test(expected = Exception.class)
	public void productoSinDescripcion() throws Exception {
		String codigo = "9999";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Le cop sportifens");		
		Producto producto = new Producto(codigo, null, precio, categoria, marca);
	}
	
	@Test(expected = Exception.class)
	public void productoSinPrecio() throws Exception {
		String codigo = "9999";
		String desripcion = "Zapas le cop sportifens";
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Le cop sportifens");
		Producto producto = new Producto(codigo, desripcion, 0, categoria, marca);
	}	
	
	/*
	 * CASO B.7
	 * Verificar que no sea posible crear un Cliente sin 
	 * dni, 
	 * nombre 
	 * apellido. 
	 * Y que el email sea v�lido
	 * */
	@Test(expected = Exception.class)
	public void cargarClinteSinNombre() throws Exception {
		Cliente cliente = new Cliente(1L, null, "chomsky", "13123123", "email@mail.com");
	}

	@Test(expected = Exception.class)
	public void cargarClinteSinApellido() throws Exception {
		Cliente cliente = new Cliente(1L, "noam", "", "13123123", "email@mail.com");
	}

	@Test(expected = Exception.class)
	public void cargarClinteSinDNI() throws Exception {
		Cliente cliente = new Cliente(1L, "noam", "chomsky", "", "email@mail.com");
	}

	@Test(expected = Exception.class)
	public void cargarClinteSinEmail() throws Exception {
		Cliente cliente = new Cliente(1L, "noam", "chomsky", "13123123", null);
	}

	@Test(expected = Exception.class)
	public void cargarClinteConDNIInvalido() throws Exception {
		Cliente cliente = new Cliente(1L, "noam", "chomsky", "13123123", "email@mailcom");
	}
	
	/*
	 * CASO B.8
	 * Verificar que no sea posible crear un descuento con fechas validez superpuestas. 
	 * */
	@Test(expected = Exception.class)
	public void crearDescuentoConFechasSuperpuestas() throws Exception {
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 31);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 8);
		LocalDate hasta2 = LocalDate.of(2022, 8, 15);
		
		//1/8 al 30/8, marca VISA - 10% de descuento para los pagos con VISA
		PromocionDePago promoPago = new PromocionDePago(desde, hasta, 0.1f, "VISA");
		//8/8 al 15/8, marca VISA - 10% de descuento para los pagos con MEMECARD		
		PromocionDePago promoPago2 = new PromocionDePago(desde2, hasta2, 0.1f, "MEMECARD");
		
		Tienda vientosPatagonicos = new Tienda("Vientos Patagonicos", new ArrayList<PromocionDeMarca>(), new ArrayList<PromocionDePago>());
		vientosPatagonicos.agregarPromocionDePago(promoPago);
		vientosPatagonicos.agregarPromocionDePago(promoPago2);	
	}			
/*
	@Test
	public void cargarPromociones() throws Exception {
		Tienda tienda = new Tienda();
		LocalDate desde = LocalDate.of(2022, 8, 1);

		PromocionTarjeta promoTarjeta1 = new PromocionTarjeta(LocalDate.of(2022, 8, 1), LocalDate.of(2022, 8, 31), 20,
				"Visa");
		PromocionTarjeta promoTarjeta2 = new PromocionTarjeta(LocalDate.of(2022, 9, 1), LocalDate.of(2022, 9, 28), 10,
				"Visa");
		tienda.cargarPromocion(promoTarjeta1);
		tienda.cargarPromocion(promoTarjeta2);
		assertEquals(1, 1);
	}

	@Test(expected = Exception.class)
	public void cargarPromocionTarjetaSuperpuestas() throws Exception {
		Tienda tienda = new Tienda();
		PromocionTarjeta promoTarjeta1 = new PromocionTarjeta(LocalDate.of(2022, 8, 25), LocalDate.of(2022, 8, 31), 20,
				"Visa");
		PromocionTarjeta promoTarjeta2 = new PromocionTarjeta(LocalDate.of(2022, 8, 28), LocalDate.of(2022, 8, 30), 10,
				"Visa");
		tienda.cargarPromocion(promoTarjeta1);
		tienda.cargarPromocion(promoTarjeta2);
	}

	@Test(expected = Exception.class)
	public void cargarPromocionTarjetaSuperpuestasExtremoDesde() throws Exception {
		Tienda tienda = new Tienda();
		PromocionTarjeta promoTarjeta1 = new PromocionTarjeta(LocalDate.of(2022, 8, 25), LocalDate.of(2022, 8, 31), 20,
				"Visa");
		PromocionTarjeta promoTarjeta2 = new PromocionTarjeta(LocalDate.of(2022, 8, 20), LocalDate.of(2022, 8, 25), 10,
				"Visa");
		tienda.cargarPromocion(promoTarjeta1);
		tienda.cargarPromocion(promoTarjeta2);
	}

	@Test(expected = Exception.class)
	public void cargarPromocionTarjetaSuperpuestasExtremoHasta() throws Exception {
		Tienda tienda = new Tienda();
		PromocionTarjeta promoTarjeta1 = new PromocionTarjeta(LocalDate.of(2022, 8, 25), LocalDate.of(2022, 8, 31), 20,
				"Visa");
		PromocionTarjeta promoTarjeta2 = new PromocionTarjeta(LocalDate.of(2022, 8, 31), LocalDate.of(2022, 9, 10), 10,
				"Visa");
		tienda.cargarPromocion(promoTarjeta1);
		tienda.cargarPromocion(promoTarjeta2);
	}

	@Test
	public void calcularMontoTotalConDescuentosCaducados() throws Exception {
		Tienda tienda = new Tienda();
		PromocionTarjeta promoTarjeta = new PromocionTarjeta(LocalDate.of(2022, 6, 25), LocalDate.of(2022, 6, 30), 20,
				"Visa");
		PromocionMarca promoMarca = new PromocionMarca(LocalDate.of(2022, 7, 31), LocalDate.of(2022, 7, 10), 10,
				"Adidas");
		tienda.cargarPromocion(promoTarjeta);
		tienda.cargarPromocion(promoMarca);
		Carrito carrito = new Carrito(new ArrayList<Producto>());
		Producto prod1 = tienda.buscarProducto("0101"); // $ 4500
		Producto prod2 = tienda.buscarProducto("0103"); // $ 8000
		carrito.addItem(prod1);
		carrito.addItem(prod2);

		Venta venta = tienda.procesarVenta(carrito, "11111111", "111222333444555", LocalDate.of(2022, 9, 1));
		System.out.println(venta.getDescuentos());
		System.out.println(venta.getSubtotal());
		System.out.println(venta.getTotal());
		assertEquals(venta.getDescuentos(), 0, 0);
		assertEquals(venta.getSubtotal(), 12500, 0);
		assertEquals(venta.getTotal(), 12500, 0);
	}

	@Test
	public void calcularMontoTotalConDescuentosVigentes() throws Exception {
		Tienda tienda = new Tienda();
		PromocionTarjeta promoTarjeta = new PromocionTarjeta(LocalDate.of(2022, 6, 25), LocalDate.of(2022, 6, 30), 20,
				"Visa");
		PromocionMarca promoMarca = new PromocionMarca(LocalDate.of(2022, 7, 31), LocalDate.of(2022, 7, 10), 10,
				"Adidas");
		tienda.cargarPromocion(promoTarjeta);
		tienda.cargarPromocion(promoMarca);
		Carrito carrito = new Carrito(new ArrayList<Producto>());
		Producto prod1 = tienda.buscarProducto("0101"); // $ 4500
		Producto prod2 = tienda.buscarProducto("0103"); // $ 8000
		carrito.addItem(prod1);
		carrito.addItem(prod2);

		Venta venta = tienda.procesarVenta(carrito, "11111111", "111222333444555", LocalDate.of(2022, 6, 26));
		System.out.println(venta.getDescuentos());
		System.out.println(venta.getSubtotal());
		System.out.println(venta.getTotal());
		assertEquals(venta.getSubtotal(), 12500, 0);
		assertEquals(venta.getDescuentos(), 2500, 0);
		assertEquals(venta.getTotal(), 10000, 0);
	}

	private ArrayList<CategoriaProducto> generarCategorias() {
		CategoriaProducto cat1 = new CategoriaProducto("Ropa deportiva");
		CategoriaProducto cat2 = new CategoriaProducto("Calzado");
		CategoriaProducto cat3 = new CategoriaProducto("Balones");
		CategoriaProducto cat4 = new CategoriaProducto("Mochilas");
		ArrayList<CategoriaProducto> categorias = new ArrayList<CategoriaProducto>();

		categorias.add(cat1);
		categorias.add(cat2);
		categorias.add(cat3);
		categorias.add(cat4);

		return categorias;
	}

	private ArrayList<Cliente> generarClientes() throws Exception {
		Cliente cli1 = new Cliente("Jose", "Pepe", "11111111", "jose@mail.com", new ArrayList<Tarjeta>());
		Cliente cli2 = new Cliente("Nico", "Perez", "22222222", "nico@mail.com", new ArrayList<Tarjeta>());
		Cliente cli3 = new Cliente("Maria", "Garcia", "11111111", "maria@mail.com", new ArrayList<Tarjeta>());
		cli1.addTarjeta("Visa", "111222333444555");
		cli2.addTarjeta("MemmeCard", "222333444555666");
		cli3.addTarjeta("Visa", "333444555666777");

		ArrayList<Cliente> clientes = new ArrayList<Cliente>();

		clientes.add(cli1);
		clientes.add(cli2);
		clientes.add(cli3);
		return clientes;
	}

	private ArrayList<Producto> generarProductos(ArrayList<CategoriaProducto> categorias) throws Exception {
		Producto prod1 = new Producto("0101", "Remera xl", 4500, "Adidas", categorias.get(0)); // Ropa
		Producto prod2 = new Producto("0102", "Pantalon", 8000, "Nike", categorias.get(0)); // Ropa deportiva
		Producto prod3 = new Producto("0103", "Campera", 8000, "Acme", categorias.get(0)); // Ropa deportiva
		Producto prod4 = new Producto("0201", "Zapatilla Nike", 20000, "Nike", categorias.get(1)); // Calazado
		Producto prod5 = new Producto("0202", "Zapatilla Adidas", 18000, "Adidas", categorias.get(1)); // Calazado
		Producto prod6 = new Producto("0203", "Bota de cuero", 30000, "Adidas", categorias.get(1)); // Calazado
		Producto prod7 = new Producto("0301", "Mochila Azul", 10000, "Acme", categorias.get(2)); // Mochilas
		Producto prod8 = new Producto("0302", "Mochila Militar", 16000, "Acme", categorias.get(2)); // Mochilas
		Producto prod9 = new Producto("0303", "Riñonera", 7500, "Acme", categorias.get(2)); // Mochilas

		ArrayList<Producto> productos = new ArrayList<Producto>();
		productos.add(prod1);
		productos.add(prod2);
		productos.add(prod3);
		productos.add(prod4);
		productos.add(prod5);
		productos.add(prod6);
		productos.add(prod7);
		productos.add(prod8);
		productos.add(prod9);
		return productos;
	}
	*/

}
