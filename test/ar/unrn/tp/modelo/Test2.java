package ar.unrn.tp.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

//import org.junit.jupiter.api.Test;

public class Test2 {

	private static String marcaTarjeta = "VISA";
	private static Tarjeta tarjeta = new Tarjeta(99L, "1111 1221 1313 1414", 123, 2018, 2023, marcaTarjeta);
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Recordar que una promocion esta desacoplada de un producto.
		 * Al carrito pasarle los descuetos del dia y chau...
		 * Recomendacion, intentar imaginar una situacion de compra real
		 * */
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		System.out.println("** Casos de Prueba **");
		System.out.println("***************************");

		casoB1();
		casoB2();
		casoB3();
		casoB4();
		casoB5();
		casoB6();
		casoB7();
		casoB8();
	}
	
	/*
	 * Calcular el monto total del carrito seleccionado sin descuentos
	 * vigentes, pero con descuentos que ya caducaron.
	 */
	private static void casoB1() {

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/2 al 28/2 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2019, 10, 1); // 2019-10-01
		LocalDate hasta = LocalDate.of(2019, 10, 30); // 2019-10-30		
		
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//8% del 1/3 al 30/3, marca VISA - 8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde, hasta, 0.08f, marcaTarjeta);
		PromocionDePago promoPago2 = new PromocionDePago(desde, hasta, 0.01f, marcaTarjeta);
		
		// Colleciones de promociones por marca
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);
		// Si ya contiene la promo
		if (promoPagos.contains(promoPago2)) {
			System.out.println("******************CONTIENE LA PROMO: " + promoPago2.toString());
		}else{
			System.out.println("****NO contiene la promo "+ promoPago2.toString());
		}
		promoPagos.add(promoPago2);
		
		
		ArrayList<PromocionDePago> items = new ArrayList<>();
		items.add(promoPago1);
		
		// Si ya contiene el producto, lo agrego igual, no manejo cantidad de producto
		if (items.contains(promoPago1)) {
			System.out.println("**** lo contiene! jah ");
			System.out.println("****PROMO PAGO: " + items.get(items.indexOf(promoPago2)).toString());
		}
		items.add(promoPago2);
		
		Iterator<PromocionDePago> itrItems = promoPagos.iterator();
		while(itrItems.hasNext()){
			PromocionDePago itemCarrito = itrItems.next();
			System.out.println("****promo pago "+itemCarrito.toString());
		}		
		
		// Carrito, no hay promociones vigentes
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjeta);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		carrito.listarProductos();
		
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.1 - Calcular el monto total del carrito. Sin descuentos vigentes, pero con descuentos que ya caducaron");		
		System.out.println("Monto total carrito: " + carrito.calcularMontoTotal());
		System.out.println("***************************");
	}

	private static void casoB2() {


		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 31);		
		
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//8% del 1/3 al 30/3, marca VISA - 8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde, hasta, 0.08f, marcaTarjeta);
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);		
		
		// Carrito, no hay promociones vigentes
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjeta);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		carrito.listarProductos();
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.2 - Calcular el monto total del carrito con un descuento vigente para los\r\n" + 
				"productos marca ADIDAS. ADEMAS CALCULO PARA PROMOPAGO VIGENTE");		
		System.out.println("Monto total carrito: " + carrito.calcularMontoTotal());
		System.out.println("***************************");			
		
	}
	
	private static void casoB3() {


		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2021, 8, 1);
		LocalDate hasta = LocalDate.of(2021, 8, 31);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 8, 31);			
		
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//8% del 1/3 al 30/3, marca VISA - 8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, marcaTarjeta);
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjeta);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		carrito.listarProductos();
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.3 - Calcular el monto total del carrito con un descuento vigente del tipo\r\n" + 
				"de Medio de pago.");		
		System.out.println("Monto total carrito: " + carrito.calcularMontoTotal());
		System.out.println("***************************");			
		
	}	
	

	private static void casoB4() {


		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 31);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 8, 31);			
		
		//marca adiddas promo
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//8% del 1/3 al 30/3, marca VISA - 8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, marcaTarjeta);
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjeta);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		carrito.listarProductos();
		
		System.out.println("***************************");
		System.out.println("CASO B.4 - Calcular el monto total del carrito con dos descuentos vigentes, sobre productos marca ADIDAS y para tarjeta de cr�dito VISA");		
		System.out.println("Monto total carrito: " + carrito.calcularMontoTotal());
		System.out.println("***************************");			
		
	}
	
	private static void casoB5() {

		// Productos
		String codigo = "1001";
		String desripcion = "Descripcion codigo 1001";
		float precio = 10;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Adidas");
		//System.out.println("Adidas ***: " + marca.toString());
		String codigo2 = "1002";
		String desripcion2 = "Descripcion codigo 1002";
		float precio2 = 20;
		Categoria categoria2 = new Categoria("Pantalones");
		Marca marca2 = new Marca("Jaguar");
		
		// Zapatilla marca Adidas
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		// Pantalon marca Jaguar
		Producto producto2 = new Producto(codigo2, desripcion2, precio2, categoria2, marca2);
		
		Producto producto3 = new Producto("1003", "Desc. producto 3", 100, new Categoria("Accesorios"), new Marca("Wilson"));
		
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 31);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 1);
		LocalDate hasta2 = LocalDate.of(2022, 8, 31);			
		
		//marca adiddas promo
		PromocionDeMarca promoMarca1 = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//8% del 1/3 al 30/3, marca VISA - 8% de descuento para los pagos con VISA
		PromocionDePago promoPago1 = new PromocionDePago(desde2, hasta2, 0.1f, marcaTarjeta);
		
		// Colleciones de promociones por marca		
		// Una promo para la marca Adidas
		Collection<PromocionDeMarca> promoMarcas = new ArrayList<PromocionDeMarca>();
		promoMarcas.add(promoMarca1);
		
		// Colleciones de promociones por marca		
		// Una promo para el pago con tarjeta
		Collection<PromocionDePago> promoPagos = new ArrayList<PromocionDePago>();
		promoPagos.add(promoPago1);				
		
		// Carrito, no hay promociones vigentes
		Carrito carrito = new Carrito(new ArrayList<Producto>(), promoMarcas, promoPagos, tarjeta);
		
		// Agregar productos al carrito con: 
		// 1 zapa adidas, y un pantalo jaguar
		carrito.agregarProducto(producto1); //precio 10
		carrito.agregarProducto(producto2); //precio 20
		
		// Listar los productos
		//carrito.listarProductos();
		
		//calcular monto, punto b.1 del ejercicio
		System.out.println("***************************");
		//System.out.println("Monto total sin descuentos: " + carrito.calcularMontoTotalSin());
		System.out.println("CASO B.5 - Realizar el pago y verificar que se genere la venta u orden de pago");
		Cliente cliente = new Cliente(1L, "Bruno Gabriel", "AMICO", "27828706", "bruno.g.amico@gmail.com");

		Tarjeta tarjetaCliente = new Tarjeta(100L, "1111222233334444", 444, 2018, 2023, "VISA");
		Tarjeta tarjetaCliente2 = new Tarjeta(100L, "1111222233335555", 555, 2018, 2023, "MASTER");
		cliente.agregarTarjeta(tarjetaCliente);
		cliente.agregarTarjeta(tarjetaCliente2);
		
		//Tarjeta tarjetaParaPagar = cliente.dameTarjeta("1111222233334444");
		//System.out.println("***************************" + tarjetaParaPagar.imprimir());
		
		//SOLO para test, al realizar la venta indico que tarjeta se va a usar... 
		//tener en cuenta que podria obtener la tarja directamente consultandole al cliente
		Venta venta = carrito.realizarVenta(cliente, cliente.dameTarjeta("1111222233334444"));
		System.out.println("Monto total carrito: " + venta.imprimir());
		System.out.println("***************************");			
		
	}	
	
	private static void casoB6() {	
		System.out.println("***************************");
		System.out.println("CASO B.6 - Verificar que no sea posible crear un Producto sin categor�a,\r\n" + 
				"descripci�n y precio.");
		// Productos
		String codigo = "9999";
		String desripcion = "Zapas le cop sportifens";
		float precio = 10000;
		Categoria categoria = new Categoria("Zapatillas");
		Marca marca = new Marca("Le cop sportifens");		
		//Producto producto = new Producto(codigo, null, precio, categoria, marca);
		Producto producto1 = new Producto(codigo, desripcion, precio, categoria, marca);
		System.out.println("Producto creado: " + producto1.imprimir());
		System.out.println("***************************");			
		
	}		
	
	private static void casoB7() {	
		System.out.println("***************************");
		System.out.println("CASO B.7 - Verificar que no sea posible crear un Cliente sin dni, nombre y\r\n" + 
				"apellido. Y que el email sea v�lido");
		// cliente	
		//Cliente cliente = new Cliente(1L, null, null, "", null);		
		System.out.println("***************************");			
	}
	
	
	private static void casoB8() {	
		System.out.println("***************************");
		System.out.println("CASO B.8 - Verificar que no sea posible crear un descuento con fechas validez\r\n" + 
				"superpuestas.");
		//5% para ADIDAS vigente desde el 1/8 al 31/8 solo test!!
		LocalDate hoy = LocalDate.now();
		LocalDate desde = LocalDate.of(2022, 8, 1);
		LocalDate hasta = LocalDate.of(2022, 8, 31);
		
		LocalDate desde2 = LocalDate.of(2022, 8, 8);
		LocalDate hasta2 = LocalDate.of(2022, 8, 15);	
		
		//marca adiddas promo
		//PromocionDeMarca promoMarca = new PromocionDeMarca(desde, hasta, 0.05f, marca);
		
		//1/3 al 30/3, marca VISA - 10% de descuento para los pagos con VISA
		PromocionDePago promoPago = new PromocionDePago(desde, hasta, 0.1f, "VISA");
		PromocionDePago promoPago2 = new PromocionDePago(desde2, hasta2, 0.1f, "MEMECARD");
		
		Tienda vientosPatagonicos = new Tienda("Vientos Patagonicos", new ArrayList<PromocionDeMarca>(), new ArrayList<PromocionDePago>());
		vientosPatagonicos.agregarPromocionDePago(promoPago);
		vientosPatagonicos.agregarPromocionDePago(promoPago2);
	
		System.out.println("***************************");			
	}		
	
}
