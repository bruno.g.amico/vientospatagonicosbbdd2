package ar.unrn.tp.api;

import java.util.List;

public interface ProductoService {
	// validar que sea una categor�a existente y que codigo no se repita
	//AGREGO la marca a la interfaz
	void crearProducto(String codigo, String descripcion, float precio, Long idCategoria, Long idMarca);

	// validar que sea un producto existente
	//void modificarProducto(Long idProducto, String descripcion);
	void modificarProducto(Long idProducto, String descripcion, float precio);

	// Devuelve todos los productos
	List listarProductos();
}