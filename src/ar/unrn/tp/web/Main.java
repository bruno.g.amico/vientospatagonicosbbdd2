package ar.unrn.tp.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.OptimisticLockException;

import org.json.JSONObject;

import ar.unrn.tp.jpa.servicios.CacheService;
import ar.unrn.tp.jpa.servicios.DefaultClienteService;
import ar.unrn.tp.jpa.servicios.DefaultDescuentoService;
import ar.unrn.tp.jpa.servicios.DefaultProductoService;
import ar.unrn.tp.jpa.servicios.DefaultVentaService;
import ar.unrn.tp.modelo.Producto;
import ar.unrn.tp.modelo.PromocionDeMarca;
import ar.unrn.tp.modelo.PromocionDePago;
import ar.unrn.tp.modelo.Tarjeta;
import io.javalin.Javalin;
import io.javalin.http.Handler;

public class Main {
	
	public static void main(String[] args) {
		
		CacheService cache = new CacheService();
		
        Javalin app = Javalin.create(config -> {
            config.enableCorsForOrigin("*");
        }).start(7071);
        app.get("/productos", traerProductos());
        app.get("/promopagos", traerPromoPagos());
        app.get("/promomarcas", traerPromoMarcas());
        app.get("/dameCliente", traerCliente(7L));
        app.get("/dameProducto", traerProducto(13L));     
        app.get("/tarjetas/{cliente}", ctx -> {
        	//TODO implementar nuevo metodo enviando el contexto como parametro, y así retornar un handler
        	var list = traerTarjetasDelCliente( Long.parseLong(ctx.pathParam("cliente")) );
        	ctx.json(Map.of("result", "success", "tarjetas", list));
        });
        app.post("/total", calcularTotal());
        app.post("/realizarCompra", realizarCompra());
        app.post("/actualizarProducto", editarProducto());
        app.get("/ultimasVentas/{id}", ultimasVentas());
        
        app.exception(Exception.class, (e, ctx) -> {
        	ctx.json(Map.of("result", "error", "message", "Ups... algo se rompió.: " + e.getMessage()));
    	});
	}
	
	/*
	 * Siempre retorno el cliente cuyo id es 1 objectdb o 7 para mysql 
	 * */
	private static Handler ultimasVentas() {		
		DefaultVentaService service = new DefaultVentaService();
		
		return ctx -> {
			
			Long idCliente  =  Long.parseLong(ctx.pathParam("id"));
			
			List<String> colleccion = service.ultimasVentas(idCliente);	

			ctx.json(Map.of("result", "success", "ventas", colleccion));
		};
	}		
	
	private static Handler editarProducto() {
		DefaultProductoService service = new DefaultProductoService();
		return ctx -> {
			
			try {			
				String body = ctx.body();
				
		        JSONObject post = new JSONObject(body);
		        
		        System.out.println("*** Realizar myJson: " + post);
		        System.out.println("*** Realizar myJson: " + post.getDouble("precio"));
		        System.out.println("*** Realizar myJson: " + post.getLong("version"));
		        
		        Long id = post.getLong("id");
		        String descripcion = post.getString("descripcion");
		        System.out.println("*** Realizar myJson: " + descripcion);
		        String codigo = post.getString("codigo");
		        String marca = post.getString("marca");
		        System.out.println("*** Realizar myJson: " + marca);
		        float precio = post.getFloat("precio");
		        System.out.println("*** Realizar myJson: " + precio);
		        Long version = post.getLong("version");
		        System.out.println("*** Realizar myJson: " + version);
	
		        
		        System.out.println("*** recibidos: " + descripcion);
		        System.out.println("*** recibidos: " + marca);
		        //System.out.println("*** recibidos: " + precioS);
		       // System.out.println("*** recibidos: " + versionS);
		        
		        if (descripcion == null || descripcion.equals("")) {
		        	throw new RuntimeException("Falta descripcion!");
		        }
	
		        if (marca == null || marca.equals("")) {
		        	throw new RuntimeException("Falta marca");
		        }
		        
		        if (codigo == null || codigo.equals("")) {
		        	throw new RuntimeException("Falta Codigo");
		        }	 		        
		        
	//	        if (precioS == null || precioS.equals("")) {
	//	        	throw new RuntimeException("Falta precio");
	//	        }
		        
		        //float precio = Float.parseFloat(precioS);
		       // Long version = Long.parseLong(versionS);
		        
		        //TODO modificar producto solo me permitia modificar dos valores...agregar alternativas
		        service.modificarProducto(id, descripcion, codigo, precio, version);
	
				ctx.json(Map.of("result", "success"));
				
			}catch(OptimisticLockException e){

				ctx.json(Map.of("result", "error", "message", "Ups (OptimisticLockException)... algo se rompió.: " + e.getMessage()));

			}catch(Exception e ){

				ctx.json(Map.of("result", "error", "message", "Ups... algo se rompió.: " + e.getMessage()));
			}				
		};
	}		

	private static Handler calcularTotal() {
		DefaultVentaService service = new DefaultVentaService();
		return ctx -> {
			String body = ctx.body();

	        JSONObject myJson = new JSONObject(body);
	        String productos = (String) myJson.get("productos");
	        String tarjetaId = (String) myJson.get("tarjetaId");
	        
	        if (productos == null || productos.equals("") || productos.equals("[]")) {
	        	throw new RuntimeException("Seleccione algun producto!");
	        }

	        if (tarjetaId == null || tarjetaId.equals("")) {
	        	throw new RuntimeException("Debe indicar la TARJETA de pago");
	        }
	        
	        productos = productos.replace("[", "");
	        productos = productos.replace("]", "");
	        
	       //comma separate String to array of Longaniza 
	        String[] elements = productos.split(",");
	        List<Long> listadoDeProductos = new ArrayList<Long>();
	        for (int i = 0; i < elements.length; i++) {   
	        	listadoDeProductos.add(Long.parseLong(elements[i]));   
	        }        
	        System.out.println("*** PRODUCTOS: " + listadoDeProductos.toString());
	        float total = service.calcularMonto(listadoDeProductos, Long.parseLong(tarjetaId));
	        System.out.println("*** resultado: " + total);
	        
			ctx.json(Map.of("result", "success", "total", total));
		};
	}
	

	private static Handler realizarCompra() {
		DefaultVentaService service = new DefaultVentaService();
		return ctx -> {
			String body = ctx.body();
			
	        JSONObject myJson = new JSONObject(body);

	        String productos = (String) myJson.get("productos");
	        String idTarjeta = (String) myJson.get("idTarjeta");
	        String idClient = (String) myJson.get("idClient");
	        
	        if (productos == null || productos.equals("") || productos.equals("[]")) {
	        	throw new RuntimeException("Seleccione algun producto!");
	        }

	        if (idTarjeta == null || idTarjeta.equals("")) {
	        	throw new RuntimeException("Debe indicar la TARJETA de pago");
	        }
	        
	        if (idClient == null || idClient.equals("")) {
	        	throw new RuntimeException("Debe indicar el CLIENTE");
	        }
	        
	        productos = productos.replace("[", "");
	        productos = productos.replace("]", "");
	        
	       //comma separate String to array of Longaniza 
	        String[] elements = productos.split(",");
	        List<Long> listadoDeProductos = new ArrayList<Long>();
	        for (int i = 0; i < elements.length; i++) {   
	        	listadoDeProductos.add(Long.parseLong(elements[i]));   
	        }        
	        System.out.println("*** Realizar Venta - PRODUCTOS: " + listadoDeProductos.toString());
	        service.realizarVenta(Long.parseLong(idClient), listadoDeProductos, Long.parseLong(idTarjeta));

			ctx.json(Map.of("result", "success"));
		};
	}	
	
	private static Handler traerProductos() {
		DefaultProductoService service = new DefaultProductoService();
			
		return ctx -> {
			var productos = service.listarProductos();
			var list = new ArrayList<Map<String, Object>>();
			for (Producto p : productos) {
				list.add(p.toMap());
			}
			ctx.json(Map.of("result", "success", "productos", list));
		};
	}
	
	private static Handler traerPromoPagos() {
		DefaultDescuentoService service = new DefaultDescuentoService();
			
		return ctx -> {
			List<PromocionDePago> colleccion = service.listarDescuentosDePago();	
			List list = new ArrayList<Map<String, Object>>();
			for (PromocionDePago obj : colleccion) {
				list.add(obj.toMap());
			}
			ctx.json(list);
		};
	}
	
	private static Handler traerPromoMarcas() {
		DefaultDescuentoService service = new DefaultDescuentoService();
			
		return ctx -> {
			List<PromocionDeMarca> colleccion = service.listarDescuentosDeMarca();	
			List list = new ArrayList<Map<String, Object>>();
			for (PromocionDeMarca obj : colleccion) {
				list.add(obj.toMap());
			}
			ctx.json(list);
		};
	}

	/*
	 * Siempre retorno el cliente cuyo id es 1 
	 * */
	private static Handler traerCliente(Long idCliente) {
		DefaultClienteService service = new DefaultClienteService();
			
		return ctx -> {
			var cliente = service.traerCliente(idCliente);
			ctx.json(Map.of("result", "success", "cliente", cliente.toMap()));
		};
	}	

	private static List traerTarjetasDelCliente(Long idCliente) {
		DefaultClienteService service = new DefaultClienteService();

		var tarjetas = service.listarTarjetas(idCliente);
		var list = new ArrayList<Map<String, Object>>();
		for (Tarjeta t : tarjetas) {
			list.add(t.toMap());
		}
		return list;

	}	
	
	/*
	 * Siempre retorno el producto cuyo id es xxx
	 * */
	private static Handler traerProducto(Long id) {
		DefaultProductoService service = new DefaultProductoService();
			
		return ctx -> {
			var producto = service.traerProducto(id);
			ctx.json(Map.of("result", "success", "producto", producto.toMap()));
		};
	}	
}
