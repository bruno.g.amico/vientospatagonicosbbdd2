package ar.unrn.tp.web;

import io.javalin.Javalin;

public class HelloWorld {
    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7071);
        app.get("/", ctx -> ctx.result("Hello World, hola proyecto!"));
    }
}