package ar.unrn.tp.web;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;
import ar.unrn.tp.modelo.Producto;
import io.javalin.Javalin;

public class ProductosAPI {

	public static void main(String[] args) {

		DefaultProductoService service = new DefaultProductoService();
		List<Producto> productos = service.listarProductos();
		
        Javalin app = Javalin.create().start(7071);
        app.get("/productos", ctx -> ctx.json(productos));

	}

}
