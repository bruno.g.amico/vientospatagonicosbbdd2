package ar.unrn.tp.main;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;
import ar.unrn.tp.modelo.Tarjeta;

/* 
 * Ejecutar antes Main03xxx
 * */
public class Main05ListarTarjetasCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();

		List<Tarjeta> tarjetas = service.listarTarjetas(1L);
		System.out.println("*** Casos de Prueba ***");
		for (Tarjeta t : tarjetas) {
			System.out.println(t.imprimir());
		}
	
	}

}

