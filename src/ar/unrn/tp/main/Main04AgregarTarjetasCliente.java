package ar.unrn.tp.main;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;

/* 
 * Ejecutar antes Main03xxx
 * */
public class Main04AgregarTarjetasCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		
		//agregar tarjetas al cliente xxx
		service.agregarTarjeta(1L, "1111222233334444", "MEMECARD");
		service.agregarTarjeta(1L, "1111222233335555", "VISSASCARD");
		service.agregarTarjeta(2L, "1111222233336666", "MEMECARD2");
	
	}

}

