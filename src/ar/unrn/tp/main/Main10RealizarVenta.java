package ar.unrn.tp.main;

import java.util.ArrayList;
import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultVentaService;

public class Main10RealizarVenta {

	public static void main(String[] args) {
		
		// --> realizar venta
		DefaultVentaService serviceVenta = new DefaultVentaService();
		
		//al cliente y sus tarjetas lo persisti con anterioridad 
		Long idCliente = 1L; //molinari
		Long idTarjeta = 4L; //MEMECARD

		// Productos
		List<Long> productos = new ArrayList<>();
		// Zapatilla marca ADIDAS
		Long producto1 = 7L;
		// Pantalon
		Long producto2 = 8L;
		// Zapas JAGUAR
		Long producto3 = 9L;
		productos.add(producto1);
		productos.add(producto2);
		productos.add(producto3);
		
		//realizo la venta de test, paso listado de IDs productos...
		//las promos las recupero en la implementacion del servicos, se supone que las promos ya existen
		//para la tienda
		serviceVenta.realizarVenta(idCliente, productos, idTarjeta);
		
		// <-- realizar venta	
	}
	
}
