package ar.unrn.tp.main;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultVentaService;
import ar.unrn.tp.modelo.Venta;

public class Main12ListarVentas {

	public static void main(String[] args) {

		DefaultVentaService serviceVenta = new DefaultVentaService();

		// listar las ventas
		List<Venta> ventas = serviceVenta.ventas();
    	System.out.println("** Casos de Prueba **");
		for (Venta v : ventas) {
			System.out.println("VENTA encontrada: " + v.imprimir());				
		}		
	}
}
