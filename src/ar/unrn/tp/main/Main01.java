package ar.unrn.tp.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ar.unrn.tp.modelo.Categoria;
import ar.unrn.tp.modelo.Marca;

/*
 * configuracion INICIAL, la tienda pordria ser una instancia sin necesidad de pesistirse
 * */
public class Main01 {

	public static void main(String[] args) {
		persistirCategoriasYMarcas();
	}

	/*
	 * Codigos de categorias de 10 a 19
	 * codigos de marca de 20 a 29
	 **/
	private static void persistirCategoriasYMarcas() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-objectdb");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();

			// Crear las categorias
			Categoria cat1 = new Categoria(10L, "Zapatillas");
			Categoria cat2 = new Categoria(11L, "Pantalones");
			Categoria cat3 = new Categoria(12L, "Accesorios");
			
			// Crear las marcas
			Marca marca1 = new Marca(20L, "ADIDAS");
			Marca marca2 = new Marca(21L, "JAGUAR");
			Marca marca3 = new Marca(22L, "WILSON");

			em.persist(marca1);
			em.persist(marca2);
			em.persist(marca3);			

			em.persist(cat1);
			em.persist(cat2);
			em.persist(cat3);

			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}			

	}

}

