package ar.unrn.tp.main;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;

public class Main06CrearProductos {

	public static void main(String[] args) {
		
		DefaultProductoService service = new DefaultProductoService();
		//Zapatillas
		Long idCategoria = 10L;
		//marca ADIDAS
		Long idMarca = 20L;
		Long idProducto = 6L;

		service.crearProducto("PROD-001", "Zapatillas Adidas", 10000, idCategoria, idMarca);
		//accsorio wilson 
		service.crearProducto("PROD-002", "Pantalones mi pantalon", 5000, 12L, 22L);
		//zapas jaguar
		service.crearProducto("PROD-003", "Zapatillas Jaguar", 20000, 10L, 21L);
		
//		service.modificarProducto(idProducto, "Zapatillas ADIDAS", 13500);
////		
//		List<Producto> productos = service.listarProductos();
//    	System.out.println("** Listando productos **");
//        for (Producto prod : productos) {
//        	System.out.println(prod.imprimir());
//        }	

	}
}
