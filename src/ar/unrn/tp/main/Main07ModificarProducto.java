package ar.unrn.tp.main;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;

public class Main07ModificarProducto {

	public static void main(String[] args) {
		
		DefaultProductoService service = new DefaultProductoService();
		Long idProducto = 7L; //zapas adidas

		service.modificarProducto(idProducto, "Zapatillas ADIDAS BOS", 15000);

	}
}
