package ar.unrn.tp.main;

import java.time.LocalDate;
import ar.unrn.tp.jpa.servicios.DefaultDescuentoService;

public class Main09CrearDescuentos {

	public static void main(String[] args) {
		
		DefaultDescuentoService service = new DefaultDescuentoService();
		
		// LocalDate hoy = LocalDate.now();
		LocalDate fechaDesde = LocalDate.of(2022, 10, 1); //2022-10-01
		LocalDate fechaHasta = LocalDate.of(2022, 10, 31); //2022-10-31
		float porcentaje = 0.1f;
		String marcaTarjeta = "MEMECARD";
		
		service.crearDescuentoSobreTotal(marcaTarjeta, fechaDesde, fechaHasta, porcentaje);
		service.crearDescuento("ADIDAS", fechaDesde, fechaHasta, porcentaje);
		//para promos de marca permito varias vigentes por el momento sin considerar su marca
		service.crearDescuento("JAGUAR", fechaDesde, fechaHasta, 0.2f);
	}

}
