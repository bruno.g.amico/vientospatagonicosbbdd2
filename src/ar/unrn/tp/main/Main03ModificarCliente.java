package ar.unrn.tp.main;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;

/* 
 * Ejecutar antes Main02xxx
 * */
public class Main03ModificarCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		
		//modificar cliente
		service.modificarCliente(1L, "Enrique");
	
	}

}

