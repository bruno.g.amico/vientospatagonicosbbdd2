package ar.unrn.tp.main;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;
import ar.unrn.tp.modelo.Producto;

public class Main08ListarProductos {

	public static void main(String[] args) {
		
		DefaultProductoService service = new DefaultProductoService();
		
		List<Producto> productos = service.listarProductos();
    	System.out.println("** Listando productos **");
        for (Producto prod : productos) {
        	System.out.println(prod.imprimir());
        }	

	}
}
