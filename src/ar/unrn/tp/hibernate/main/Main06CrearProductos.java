package ar.unrn.tp.hibernate.main;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;

/* 
 * Ejecutar antes Main05xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * */
public class Main06CrearProductos {

	public static void main(String[] args) {
		
		DefaultProductoService service = new DefaultProductoService();
		//Zapatillas
		Long idCategoria = 4L;
		//marca ADIDAS
		Long idMarca = 1L;

		service.crearProducto("PROD-001", "Zapatillas Adidas", 10000, idCategoria, idMarca);
		//accesorio marca wilson 
		service.crearProducto("PROD-002", "Pantalones mi pantalon", 5000, 6L, 3L);
		//zapas marca jaguar
		service.crearProducto("PROD-003", "Zapatillas Jaguar", 20000, 4L, 2L);
		
//		service.modificarProducto(idProducto, "Zapatillas ADIDAS", 13500);
////		
//		List<Producto> productos = service.listarProductos();
//    	System.out.println("** Listando productos **");
//        for (Producto prod : productos) {
//        	System.out.println(prod.imprimir());
//        }	

	}
}
