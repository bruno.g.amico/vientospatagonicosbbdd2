package ar.unrn.tp.hibernate.main;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultVentaService;
import ar.unrn.tp.modelo.Venta;

/* 
 * Ejecutar antes Main12xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * */
public class Main12ListarVentas {

	public static void main(String[] args) {

		DefaultVentaService serviceVenta = new DefaultVentaService();

		// listar las ventas
		List<Venta> ventas = serviceVenta.ventas();
    	System.out.println("** Casos de Prueba **");
		for (Venta v : ventas) {
			System.out.println("VENTA encontrada: " + v.imprimir());				
		}		
	}
}
