package ar.unrn.tp.hibernate.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ar.unrn.tp.modelo.Categoria;
import ar.unrn.tp.modelo.Marca;
import ar.unrn.tp.modelo.NextNumber;

/*
 * configuracion INICIAL de la numeracion
 * */
public class Main13 {

	public static void main(String[] args) {
		persistirCategoriasYMarcas();
	}

	/*
	 * Codigos de categorias de 10 a 19
	 * codigos de marca de 20 a 29
	 **/
	private static void persistirCategoriasYMarcas() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-mysql");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			
			//inicio de la numeracion para el 2022
			NextNumber numeracion = new NextNumber(2022);
			em.persist(numeracion);

			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}			

	}

}

