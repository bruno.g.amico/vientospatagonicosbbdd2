package ar.unrn.tp.hibernate.main;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;

/* 
 * Ejecutar antes Main03xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * 
 * */
public class Main04AgregarTarjetasCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		
		//agregar tarjetas al cliente xxx
		service.agregarTarjeta(7L, "1111222233334444", "MEMECARD");
		service.agregarTarjeta(7L, "1111222233335555", "VISSASCARD");
		service.agregarTarjeta(8L, "1111222233336666", "MEMECARD2");
	
	}

}

