package ar.unrn.tp.hibernate.main;

import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;
import ar.unrn.tp.modelo.Tarjeta;

/* 
 * Ejecutar antes Main03xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * */
public class Main05ListarTarjetasCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		
		//7L enrique
		List<Tarjeta> tarjetas = service.listarTarjetas(7L);
		System.out.println("*** Casos de Prueba ***");
		for (Tarjeta t : tarjetas) {
			System.out.println(t.imprimir());
		}
	
	}

}

