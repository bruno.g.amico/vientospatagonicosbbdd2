package ar.unrn.tp.hibernate.main;

import java.util.ArrayList;
import java.util.List;

import ar.unrn.tp.jpa.servicios.DefaultVentaService;

/* 
 * Ejecutar antes Main10xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * */

public class Main11CalcularMontoVenta {

	public static void main(String[] args) {
		
		// --> realizar venta
		DefaultVentaService serviceVenta = new DefaultVentaService();
		
		//al cliente y sus tarjetas lo persisti con anterioridad 
		Long idCliente = 7L; //molinari
		Long idTarjeta = 10L; //MEMECARD

		// Productos
		List<Long> productos = new ArrayList<>();
		// Zapatilla marca ADIDAS
		Long producto1 = 13L;
		// Pantalon
		Long producto2 = 14L;
		// Zapas JAGUAR
		Long producto3 = 15L;
		productos.add(producto1);
		productos.add(producto2);
		productos.add(producto3);
		
		//realizo la venta de test, paso listado de IDs productos...
		//las promos las recupero en la implementacion del servicos, se supone que las promos ya existen
		//para la tienda
		float total = serviceVenta.calcularMonto(productos, idTarjeta);
		System.out.println("** TOTAL de la venta: " + total);
	
		// --> calcular monto total
	}
	
}
