package ar.unrn.tp.hibernate.main;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;

/* 
 * Ejecutar antes Main01
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * 
 * */
public class Main02AltaClientes {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		service.crearCliente("Quique", "MOLINARI", "25126126", "emoli@mail.com");
		service.crearCliente("Ayrton Juan Manuel", "FANGELOTE", "25126127", "ajmf@mail.com");
		service.crearCliente("Bruno Gabriel", "D'AMICO", "27828828", "ba@mail.com");
	
	}

}

