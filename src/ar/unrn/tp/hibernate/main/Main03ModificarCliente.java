package ar.unrn.tp.hibernate.main;

import ar.unrn.tp.jpa.servicios.DefaultClienteService;

/* 
 * Ejecutar antes Main02xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * 
 * */
public class Main03ModificarCliente {

	public static void main(String[] args) {

		// alta de cliente
		DefaultClienteService service = new DefaultClienteService();
		
		//modificar cliente
		//service.modificarCliente(1L, "Enrique");
		//en hibernate le asigno el id 7
		service.modificarCliente(7L, "Enrique");
	
	}

}

