package ar.unrn.tp.hibernate.main;

import ar.unrn.tp.jpa.servicios.DefaultProductoService;

/* 
 * Ejecutar antes Main06xxx
 * 
 * Verificar <property name="hibernate.hbm2ddl.auto" value="update" /> 
 * */
public class Main07ModificarProducto {

	public static void main(String[] args) {
		
		DefaultProductoService service = new DefaultProductoService();
		Long idProducto = 13L; //zapas marca adidas

		service.modificarProducto(idProducto, "Zapatillas ADIDAS-BOS", 15000);

	}
}
