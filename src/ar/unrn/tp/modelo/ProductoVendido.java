package ar.unrn.tp.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author ll
 *
 */
@Entity
public class ProductoVendido{
	@Id
	@GeneratedValue	
	private Long id;	
	private String codigo;
	private String descripcion;
	private float precio;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Categoria categoria;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Marca marca;
		
	public ProductoVendido(String codigo, String descripcion, float precio, Categoria categoria, Marca marca) {
		validar(codigo, descripcion, precio, categoria, marca);
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.categoria = categoria;
		this.marca = marca;
	}
	
	private void validar(String codigo, String descripcion, float precio, Categoria categoria, Marca marca) {			
		if (codigo.equals("")) {
			throw new RuntimeException("Debe ingresar un Codigo");
		}	
		if (precio == 0) {
			throw new RuntimeException("Debe ingresar un Precio");
		}		
		if (descripcion.equals("")) {
			throw new RuntimeException("Debe ingresar una Descripcion");
		}
		if(categoria == null){
			throw new RuntimeException("Debe indicar la Categoria");
		}
		if(marca == null){
			throw new RuntimeException("Debe indicar la Marca");
		}			
	}

	public String getCodigo() {
		return this.codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Categoria getCategoria() {
		return this.categoria;
	}

	public Marca getMarca() {
		return this.marca;
	}

	public float getPrecio() {
		return this.precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean esDeCategoria(Categoria categoria) {
		return this.categoria.equals(categoria);
	}

	public boolean esDeMarca(Marca marca) {
		return this.marca.equals(marca);
	}
	
	public String imprimir() {
		return "ProductoVendido [codigo=" + codigo + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", marca=" + this.marca.getDescripcion() + "]";
	}
}
