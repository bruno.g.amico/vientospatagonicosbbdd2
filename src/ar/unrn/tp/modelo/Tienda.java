package ar.unrn.tp.modelo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class Tienda{
	
	private Long id;
	private String nombre;
	private Collection<PromocionDeMarca> promoMarcas = new ArrayList<>();
	private Collection<PromocionDePago> promoPagos = new ArrayList<>();
	
	protected Tienda() {
		
	}
    private Long getId() {
        return id;
    }
    
    private void setId(Long id) {
        this.id = id;
    }
	
	public Tienda(String nombre, 
			Collection<PromocionDeMarca> promoMarcas,
			Collection<PromocionDePago> promoPagos) 
	{
		this.nombre = nombre;
		this.promoMarcas = promoMarcas;
		this.promoPagos = promoPagos;
	}

	public String imprimir() {
		return "[Nombre =" + this.nombre

				+ "\n , promociones por marcas = \n"    

				+ this.listarPromosMarcas()

				+ "\n , promociones por pagos = "
				
				+ this.listarPromosPagos()

				+ "]";				
	}
	
	public void agregarPromocionDeMarcas(PromocionDeMarca promo) {
		this.promoMarcas.add(promo);
	}
	
	public void agregarPromocionDePago(PromocionDePago promo) {
		// Si ya contiene la promo
		if (promoPagos.contains(promo)) {
			throw new RuntimeException("La promocion ya existe");
		}else{
			Iterator<PromocionDePago> itrItems = this.promoPagos.iterator();
			while(itrItems.hasNext()){
				PromocionDePago itemPromo = itrItems.next();
				if (itemPromo.isPromoVigente()) {
					throw new RuntimeException("Ya existe una promocion VIGENTE, solo una promocion de pago es posible");
				}else if (this.seSuperponen(promo, itemPromo) ){
					throw new RuntimeException("La promocion se SUPERPONE con otra");
				}
				//System.out.println("****listando el pago " + itemPromo.toString());
			}					
			this.promoPagos.add(promo);
			//System.out.println("****agregamos promo pago "+promo.toString());
		}	
	}		
	
	private Date toDate(LocalDate fecha) {
		return java.util.Date.from(fecha.atStartOfDay()
			      .atZone(ZoneId.systemDefault())
			      .toInstant());
	}
	
	private LocalDate toLocalDate(Date fecha) {
		return fecha.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
	}
	
	private boolean seSuperponen(PromocionDePago promo, PromocionDePago promoActual) {
		Date promoDesde = toDate(promo.getDesde());
		Date promoActualDesde = toDate(promoActual.getDesde());
		Date promoHasta = toDate(promo.getHasta());
		Date promoActualHasta = toDate(promoActual.getHasta());
		if (promoDesde.after(promoActualDesde) && promoDesde.before(promoActualHasta)
			|| (promoHasta.after(promoActualDesde) && promoHasta.before(promoActualHasta))
			|| promoDesde.before(promoActualDesde) && promoHasta.after(promoActualHasta)) {
			return true;
		}		
		return false;
	}

	private String listarPromosMarcas() {
		String cadena = " [ ";
		Iterator<PromocionDeMarca> iter = this.promoMarcas.iterator();
		while (iter.hasNext()){
			cadena += iter.next().toString();
		}
		cadena += " ] ";
		return cadena;		
	}
	
	private String listarPromosPagos() {
		String cadena = " [ ";
		Iterator<PromocionDePago> iter = this.promoPagos.iterator();
		while (iter.hasNext()){
			cadena += iter.next().toString();
		}
		cadena += " ] ";
		return cadena;		
	}

	public String getNombre() {
		return this.nombre;
	}	
}
