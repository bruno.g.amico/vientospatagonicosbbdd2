/**
 * 
 */
package ar.unrn.tp.modelo;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

/*
 * De los productos se conoce su c�digo que lo
 * identifica un�vocamente, una descripci�n, una categor�a y un precio. Existen
 * varias categor�as entre las cuales tenemos: Ropa deportiva, Calzado, etc.
 * 
 * NOTA no considero la cantidad de un producto x en el carrito como un atributo, si hay dos productos iguales
 * entonces agrego dos productos al carrito
 * */


@Entity
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class Producto {
	@Id
	@GeneratedValue
	private Long id;
	private String codigo;
	private String descripcion;
	private float precio;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Categoria categoria;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Marca marca;
	
	@Version
	private Long version; 

	protected Producto() {
		
	}
	/*
	 * Es para test, del tp5
	 * 
	 * TODO falta categoria y marca
	 **/
	public Producto(Long id, String descripcion, String codigo, float precio, Long version) {
		validar(codigo, descripcion, precio);
		this.id = id;
		this.descripcion = descripcion;
		this.codigo = codigo;
		this.precio = precio;
		this.version = version;	
	}
	
	public Producto(Long id, String codigo, String descripcion, float precio, Categoria categoria, Marca marca) {
		validar(codigo, descripcion, precio, categoria, marca);
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.categoria = categoria;
		this.marca = marca;
	}

	public Producto(Long id, String codigo, String descripcion, float precio, Categoria categoria) {
		validar(codigo, descripcion, precio);
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.categoria = categoria;
	}
	
	public Producto(String codigo, String descripcion, float precio, Categoria categoria) {
		validar(codigo, descripcion, precio);
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.categoria = categoria;
	}
	
	public Producto(String codigo, String descripcion, float precio, Categoria categoria, Marca marca) {
		validar(codigo, descripcion, precio, categoria, marca);
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.categoria = categoria;
		this.marca = marca;
	}

	private void validar(String codigo, String descripcion, float precio) {
		if (codigo == null || codigo.equals("")) {
			throw new RuntimeException("Debe ingresar un Codigo");
		}
		if (precio == 0) {
			throw new RuntimeException("Debe ingresar un Precio");
		}
		if (descripcion == null || descripcion.equals("")) {
			throw new RuntimeException("Debe ingresar una Descripcion");
		}
	}

	private void validar(String codigo, String descripcion, float precio, Categoria categoria, Marca marca) {
		this.validar(codigo, descripcion, precio);
		if (categoria == null) {
			throw new RuntimeException("Debe indicar la Categoria");
		}
		if (marca == null) {
			throw new RuntimeException("Debe indicar la Marca");
		}
	}
	
	public String getCodigo() {
		return this.codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Categoria getCategoria() {
		return this.categoria;
	}	

	public float getPrecio() {
		return this.precio;
	}
	
	public Marca getMarca() {
		return this.marca;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean esDeCategoria(Categoria categoria) {
		return this.categoria.equals(categoria);
	}

	public boolean esDeMarca(Marca m) {
		return this.marca.equals(m);
	}
	
	public String imprimir() {
		return "[id = " + this.id 
				+ " codigo =" + this.codigo 
				+ ", descripcon =" + this.descripcion
				+ ", precio =" + this.precio
				+ ", categoria =" + this.categoria.imprimir() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marca other = (Marca) obj;
		if (this.descripcion != other.getDescripcion())
			return false;
		return true;
	}

	public Map<String, Object> toMap() {
		var map = new HashMap<String, Object>(
			Map.of("id", id, "codigo", codigo, "descripcion", descripcion, "precio", precio, "version", version)
		);
		
		if (this.marca != null) {
			map.put("marca", this.marca.toMap());
		}
		return map;
	}
}
