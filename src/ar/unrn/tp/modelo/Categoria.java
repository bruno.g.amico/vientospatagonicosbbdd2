/**
 * 
 */
package ar.unrn.tp.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Categoria{
	
	@Id @GeneratedValue
	private Long id;
	private String descripcion;
	
	protected Categoria() {
		
	}
	
    private Long getId() {
        return id;
    }
    
    private void setId(Long id) {
        this.id = id;
    }

    private String getDescripcion() {
        return descripcion;
    }
    
    private void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
	public Categoria(Long id, String descripcion) {
		this.descripcion = descripcion;
		this.id = id;
	}
	
	public Categoria(String descripcion) {
		this.descripcion = descripcion;
	}

	public String nombre() {
		return this.descripcion;
	}
	
	public String imprimir() {
		return "[id =" + id + " descripcion =" + this.descripcion + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		return this.descripcion.equals(other.descripcion);
	}

	public Long primaryKey() {
		return this.id;
	}
}
