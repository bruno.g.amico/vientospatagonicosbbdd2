package ar.unrn.tp.modelo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("PM")
public class PromocionDeMarca extends Promocion{

	private float porcentajeDesc = 0.05f;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Marca marca;

	
	protected PromocionDeMarca() {
		
	}
	
	public PromocionDeMarca(Long id, LocalDate desde, LocalDate hasta, float porcentajeDesc, Marca marca) {
		super(id, desde, hasta, porcentajeDesc);
		this.porcentajeDesc = porcentajeDesc;
		this.marca = marca;
	}
	
	public PromocionDeMarca(LocalDate desde, LocalDate hasta, float porcentajeDesc, Marca marca) {
		super(desde, hasta, porcentajeDesc);
		this.porcentajeDesc = porcentajeDesc;
		this.marca = marca;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public float getPorcentajeDesc() {
		return porcentajeDesc;
	}

	public void setPorcentajeDesc(float porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public float aplicarPromocion(Producto producto) {
		float descuento = 0;	
		if (producto.esDeMarca(this.marca) && this.isPromoVigente()) {
			descuento = producto.getPrecio() * this.porcentajeDesc;
		}
		return descuento;
	}

	public String imrpimir() {
		return "[desde =" + this.desde + ", hasta =" + this.hasta + ", porcenta de descuento =" + this.porcentajeDesc
				+ ", marca =" + this.marca.toString() + "]";
	}
	
	public Map<String, Object> toMap() {
		HashMap<String, Object>  map = new HashMap<String, Object>();

		map.put("desde", this.toLocalDate(this.desde).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		map.put("hasta", this.toLocalDate(this.hasta).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		map.put("porcentaje", this.porcentajeDesc);
		map.put("tarjeta", this.marca.getDescripcion());
		
		return map;
	}			

}