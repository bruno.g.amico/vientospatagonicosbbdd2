package ar.unrn.tp.modelo;

import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Marca{
	
	@Id
	@GeneratedValue	
	private Long id;
	private String descripcion;
	
	protected Marca() {
		
	}
	
    private long getId() {
        return id;
    }
    
    private void setId(Long id) {
        this.id = id;
    }	
	
	public Marca(Long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	public Marca(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}
	
	public Long primaryKey() {
		return this.id;
	}	
	
	public String imprimir() {
		return "[id =" + id + " descripcion =" + this.descripcion + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Marca other = (Marca) obj;
		return this.descripcion.equals(other.descripcion);
	}

	public Map<String, Object> toMap() {
		return Map.of("id", id, "descripcion", descripcion);
	}
}
