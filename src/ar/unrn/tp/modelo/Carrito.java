/**
 * 
 */
package ar.unrn.tp.modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ar.unrn.tp.jpa.servicios.CacheService;

/**
 * @author ll
 *
 */
public class Carrito{

	private List<Producto> productos = new ArrayList<>();
	private Collection<PromocionDeMarca> promoDeMarcas = new ArrayList<>();
	private Collection<PromocionDePago> promoDePagos = new ArrayList<>();
	private Tarjeta tarjeta;
	private CacheService cache;

	public Carrito(List<Producto> productos, Collection<PromocionDeMarca> promoMarcas, Collection<PromocionDePago> promoPagos) {
		this.productos = productos;
		this.promoDeMarcas = promoMarcas;
		this.promoDePagos = promoPagos;
		this.cache = new CacheService();
	}

	public Carrito(List<Producto> productos, Collection<PromocionDeMarca> promoMarcas, Collection<PromocionDePago> promoPagos, Tarjeta tarjeta) {
		this.productos = productos;
		this.promoDeMarcas = promoMarcas;
		this.promoDePagos = promoPagos;
		this.tarjeta = tarjeta;
	}

	/*
	 * agrega un producto al carrito.
	 * 
	 */
	public void agregarProducto(Producto p) {
		// por ahora permite duplicar productos
		this.productos.add(p);
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void listarProductos() {

		Iterator<Producto> iter = this.productos.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next().imprimir());
		}
	}

	/**
	 * Calculo el monto total del carrito
	 */
	public float calcularMontoTotal(Tarjeta tarjeta) {
		float total = 0;
		Iterator<Producto> iter = this.productos.iterator();
		while (iter.hasNext()) {
			Producto prod = iter.next();
			float totalDescuentos = 0;
	        for (PromocionDeMarca promoM : this.promoDeMarcas) {
	        	totalDescuentos += promoM.aplicarPromocion(prod);
	        }
	        total += prod.getPrecio() - totalDescuentos;
		}
		//obtener promo de pagos vigente para aplicar descuento por total de compra
		PromocionDePago promoDePagoVigente = null;
		for (PromocionDePago promo : this.promoDePagos) {
			if (promo.isPromoVigente()) {
				System.out.println("Hay promo vigente! " + promo.imprimir() + " ** " + promo.getMarca() + " ** " +tarjeta.getMarca());
				promoDePagoVigente = promo;
				//verificar la tarjeta del cliente...
				if (promo.getMarca().equals(tarjeta.getMarca())) {
					total = promoDePagoVigente.aplicarPromocion(total);
				}
			}
		}	

		return total;
	}

	/* solo test, para probar los demas casos pagando sin tarjeta */
	public float calcularMontoTotal() {

		float total = 0;
		Iterator<Producto> iter = this.productos.iterator();
		while (iter.hasNext()) {
			Producto prod = iter.next();
			float totalDescuentos = 0;
	        for (PromocionDeMarca promoM : this.promoDeMarcas) {
	        	totalDescuentos += promoM.aplicarPromocion(prod);
	        }
	        total += prod.getPrecio() - totalDescuentos;
		}
		//obtener promo de pagos vigente para aplicar descuento por total de compra
		PromocionDePago promoDePagoVigente = null;
		for (PromocionDePago promo : this.promoDePagos) {
			if (promo.isPromoVigente()) {
				promoDePagoVigente = promo;
				total = promoDePagoVigente.aplicarPromocion(total);
			}
		}

		return total;
	}

	/**
	 * realiza la venta considerando una instancia de cliente
	 * @param identificadorUnico 
	 * 
	 * */
	public Venta realizarVenta(Cliente cliente, Tarjeta tarjeta, String identificadorUnico) {
		
		System.out.println("*** INICIO DE REALIZAR VENTA ***");
		Calendar calendario = Calendar.getInstance();
		Date fechaHoraVenta = calendario.getTime();

		Collection<ProductoVendido> productosVendidos = new ArrayList<>();
		for (Producto prod : this.getProductos()) {
			ProductoVendido prodVendido = new ProductoVendido(prod.getCodigo(), prod.getDescripcion(), prod.getPrecio(),
					prod.getCategoria(), prod.getMarca());
			productosVendidos.add(prodVendido);
		}
		System.out.println("*** Se agregaron todos los productos***");
		Venta venta = new Venta(fechaHoraVenta, cliente, productosVendidos, this.calcularMontoTotal(tarjeta), identificadorUnico);
		System.out.println("*** Cree la venta!!!");
		
		return venta;
	}
	
	/**
	 * realiza la venta considerando un idCliente
	 * */
//	public Venta realizarVenta(Long idCliente, Tarjeta tarjeta) {
//		Calendar calendario = Calendar.getInstance();
//		Date fechaHoraVenta = calendario.getTime();
//
//		Collection<ProductoVendido> productosVendidos = new ArrayList<>();
//		for (Producto prod : this.getProductos()) {
//			ProductoVendido prodVendido = new ProductoVendido(prod.getCodigo(), prod.getDescripcion(), prod.getPrecio(),
//					prod.getCategoria(), prod.getMarca());
//			productosVendidos.add(prodVendido);
//		}
//
//		Venta venta = new Venta(fechaHoraVenta, cliente, productosVendidos, this.calcularMontoTotal(tarjeta));
//		return venta;
//	}	
}
