package ar.unrn.tp.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/*
 * 
 * Los clientes que quieren realizar alguna compra deben registrarse en el
 * sistema, de los cuales se necesita tener su nombre, apellido, DNI e email y
 * sus tarjetas de cr�dito
 *
 * */
@Entity
public class Cliente {
	
	@Id
	@GeneratedValue	
	private Long id;
	private String nombres;
	private String apellido;
	private String dni;
	private String email;
	//Embedded
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "tarjeta_id")
	private List<Tarjeta> tarjetas = new ArrayList<>();	

	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


	protected Cliente() {
		
	}
	
	public Cliente(Long idCliente, String nombres, String apellido, String dni, String email) {
		validar(nombres, apellido, dni, email);
		this.id = idCliente;
		this.nombres = nombres;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
	}
	
	public Cliente(String nombres, String apellido, String dni, String email) {
		validar(nombres, apellido, dni, email);
		this.nombres = nombres;
		this.apellido = apellido;
		this.dni = dni;
		this.email = email;
	}	

	public Long getClaveCliente() {
		return this.id;
	}
	
	public void setNombres(String nombre) {
		this.nombres = nombre;
	}

	private void setApellido(String apellido) {
		this.apellido = apellido;		
	}

	private void setDni(String dni) {
		this.dni = dni;
	}	
	
	private void setEmail(String email) {
		this.email = email;
	}	
	
	private String getNombres() {
		return this.nombres;
	}
		
	private String getApellido() {
		return this.apellido;		
	}

	private String getDni() {
		return this.dni;
	}	
	
	private String getEmail() {
		return this.email;
	}	

	private void validar(String nombres, String apellido, String dni, String email) {
		if (nombres == null || nombres.equals("")) {
			throw new RuntimeException("Debe ingresar un Nombre");
		}
		if (apellido == null || apellido.equals("")) {
			throw new RuntimeException("Debe ingresar un Apellido");
		}
		if (dni == null || dni.equals("")) {
			throw new RuntimeException("Debe ingresar un DNI");
		}
		if (email == null || email.equals("")) {
			throw new RuntimeException("Debe ingresar un Email");
		}
		if (!validateEmail(email)) {
			throw new RuntimeException("Debe ingresar un Email valido.");
		}
	}

	private boolean validateEmail(String email) {
		Pattern pattern = Pattern.compile(PATTERN_EMAIL);

		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	private boolean isEmptyTarjetas() {
		return (this.tarjetas == null || this.tarjetas.isEmpty());
	}
	
	public void agregarTarjeta(Tarjeta tarjeta) {
		// por ahora permite duplicados
		this.tarjetas.add(tarjeta);
	}
	
	public Tarjeta dameTarjeta(String numero) {
		if (!this.isEmptyTarjetas()) {
		    for (Tarjeta tarjeta : this.tarjetas) {
		        if (tarjeta.getNro().equals(numero)) {
		            return tarjeta;
		        }
		    }
		}
		return null;
	}
	
	public Tarjeta dameTarjetaById(Long idTarjeta) {
		if (!this.isEmptyTarjetas()) {
		    for (Tarjeta tarjeta : this.tarjetas) {
		        if (tarjeta.getId().equals(idTarjeta)) {
		            return tarjeta;
		        }
		    }
		}
		return null;
	}	

	public List<Tarjeta> dameLasTarjeta() {
		return this.tarjetas;
	}
	public String imprimir() {
		return "Cliente [Nombres = " 
				+ this.nombres + ", Apellido = " 
				+ this.apellido + ", DNI = "
				+ this.dni + ", Email = " 
				+ this.email + "]";
	}
		
	public Map<String, Object> toMap() {
		var map = new HashMap<String, Object>(
			Map.of("id", id, "nombres", nombres, "apellido", apellido)
		);
		
		return map;
	}	
}
