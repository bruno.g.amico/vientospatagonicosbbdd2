package ar.unrn.tp.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class NextNumber {
	@Id
	@GeneratedValue	
	private Long id;
	private int actual;
	private int anio;
	
	protected NextNumber() {}
	
	public NextNumber(int anio) {
		this.actual = 0;
		this.anio	= anio;
	}
	
	public NextNumber(int actual, int anio) {
		this.actual = actual;
		this.anio	= anio;
	}
	
	public int recuperarSiguiente() {
		this.actual += 1;
		return this.actual;
	}

	private Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	private int getActual() {
		return actual;
	}

	private void setActual(int actual) {
		this.actual = actual;
	}

	private int getAnio() {
		return anio;
	}

	private void setAnio(int anio) {
		this.anio = anio;
	}

}
