package ar.unrn.tp.modelo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PP")
public class PromocionDePago extends Promocion{

	private String marca; // marca de la tarjeta

	protected PromocionDePago() {
		
	}
	
	
	/* TODO
	 * VALIDAR que solo una puede estar vigente 
	 */
	public PromocionDePago(Long id, LocalDate desde, LocalDate hasta, float porcentajeDesc, String marca) {
		super(id, desde, hasta, porcentajeDesc);
		this.validar(marca);		
		this.marca = marca;
	}

	public PromocionDePago(LocalDate desde, LocalDate hasta, float porcentajeDesc, String marca) {
		super(desde, hasta, porcentajeDesc);
		this.validar(marca);		
		this.marca = marca;
	}
	
	public String getMarca() {
		return marca;
	}
	
	private void setMarca(String marca) {
		this.marca = marca;
	}
	
	
	private void validar(String marca) {
		if (marca.equals("")) {
			throw new RuntimeException("Debe ingresar la marca de la Tarjeta");
		}
	}

	public float aplicarPromocion(float precio, String marca) {
		if (this.marca.equals(marca)) {
			System.out.println("idems");
		}

		if (this.isPromoVigente() && this.marca.equals(marca)) {
			return precio - (precio * this.porcentajeDesc);
		}

		return precio;
	}

	public String imprimir() {
		return "[desde =" + this.desde + ", hasta =" + this.hasta + ", porcenta de descuento =" + this.porcentajeDesc + ", marca = " + this.marca
				+ "]";
	}
	
	public float aplicarPromocion(float total) {
		float descuento = 0;
		if (this.isPromoVigente()) {
			descuento = total * this.porcentajeDesc;
		}

		return total - descuento;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PromocionDePago other = (PromocionDePago) obj;
		if (this.marca != other.getMarca())
			return false;
		return true;
	}

	public Map<String, Object> toMap() {
		HashMap<String, Object>  map = new HashMap<String, Object>();

		map.put("desde", this.toLocalDate(this.desde).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		map.put("hasta", this.toLocalDate(this.hasta).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		map.put("porcentaje", this.porcentajeDesc);
		map.put("tarjeta", this.marca);
		
		return map;
	}		
}