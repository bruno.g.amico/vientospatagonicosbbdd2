/**
 * 
 */
package ar.unrn.tp.modelo;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author ll
 *
 */
@Entity
public class Tarjeta{
	
	@Id
	@GeneratedValue		
	private Long id;
	private String nro;
	private int codigo;
	private int desde;
	private int hasta;
	private String marca; // VISA, MASTERCARD, BOSTACARD//
	
	protected Tarjeta() {
		
	}
	
	public Tarjeta(Long id, String nro, int codigo, int desde, int hasta, String marca) {
		this.id = id;
		this.nro = nro;
		this.codigo = codigo;
		this.desde = desde;
		this.hasta = hasta;
		this.marca = marca;
	}

	public Tarjeta(String nro, int codigo, int desde, int hasta, String marca) {
		this.validar(nro, codigo, desde, hasta, marca);
		this.nro = nro;
		this.codigo = codigo;
		this.desde = desde;
		this.hasta = hasta;
		this.marca = marca;
	}
	
	public Tarjeta(Long idTarjeta, String nro, String marca) {
		this.id = idTarjeta;
		this.nro = nro;
		this.marca = marca;
	}
	
	public Tarjeta(String nro, String marca) {
		//validacion para ajustarse a la interfaz
		this.validarNroMarca(nro, marca);
		this.nro = nro;
		this.marca = marca;
	}

	private void validarNroMarca(String nro, String marca) {
		if (nro == null || nro.equals("")) {
			throw new RuntimeException("Debe indicar el nro. de la tarjeta");
		}
		if (marca == null || marca.equals("")) {
			throw new RuntimeException("Debe indicar la marca de la tarjeta");
		}		
	}		
	
	private void validar(String nro, int codigo, int desde, int hasta, String marca) {
		this.validar(nro, codigo, desde, hasta, marca);
		if (nro == null || nro.equals("")) {
			throw new RuntimeException("Debe indicar el nro. de la tarjeta");
		}
		if (codigo <= 0 ) {
			throw new RuntimeException("Debe ingresar un codigo de serguridad mayor a 0 (cero)");
		}
		if (desde >= 0 && desde <= 12) {
			throw new RuntimeException("Rango de meses posibles [1-12]");
		}
		if (hasta >= 0 && hasta <= 12) {
			throw new RuntimeException("Rango de meses posibles [1-12]");
		}
		if (marca == null || marca.equals("")) {
			throw new RuntimeException("Debe indicar la marca de la tarjeta");
		}		
	}	

	public String getMarca() {
		return marca;
	}

	public String getNro() {
		return this.nro;
	}
	
	public Long getId() {
		return this.id;
	}

	public String imprimir() {
		return "Tarjeta [id=" + id + ", nro=" + nro + ", codigo=" + codigo + ", desde=" + desde + ", hasta=" + hasta
				+ ", marca=" + marca + "]";
	}	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarjeta other = (Tarjeta) obj;
		if (this.marca != other.getMarca())
			return false;
		return true;
	}			
	
	public Map<String, Object> toMap() {
		var map = new HashMap<String, Object>(
			Map.of("id", id, "numero", nro)
		);
		
		if (this.marca != null) {
			map.put("marca", this.marca);
		}
		
		return map;
	}
}
