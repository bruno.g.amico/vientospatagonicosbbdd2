/**
 * 
 */
package ar.unrn.tp.modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author ll
 *
 */
//TODO metodo de pago es siempre por tarjeta de credito.
@Entity
public class Venta{
	@Id
	@GeneratedValue	
	private Long id;
	private Date fechaHora;
	//@ Embedded
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "venta_id")	
	private Collection<ProductoVendido> productos = new ArrayList<>();
	
	//Metodo de pago no se usa, siempre es con tarjeta
	//@ Embedded
	//private MetodoDePago MetodoDePago;
	
	@ManyToOne
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	private float monto;
	private String identificadorUnico;
	
	protected Venta() {
		
	}
	
    private Long getId() {
        return id;
    }
    
    private void setId(Long id) {
        this.id = id;
    }
	
	
	public Venta(Date fechaHora,  Cliente cliente, Collection<ProductoVendido> productosVendidos, float monto, String identificadorUnico) 
	{
		this.fechaHora = fechaHora;
		this.cliente = cliente;		
		this.productos = productosVendidos;
		this.monto = monto;
		this.identificadorUnico = identificadorUnico;
	}

	public Date getFechaHora() {
		return this.fechaHora;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}

	public Collection<ProductoVendido>  getProductos() {
		return this.productos;
	}
	
	public float getMonto() {
		return this.monto;
	}

	public void setMonto(float monto) {
		this.monto = monto;
	}

	public String getIdentificadorUnico() {
		return identificadorUnico;
	}

	public void setIdentificadorUnico(String identificadorUnico) {
		this.identificadorUnico = identificadorUnico;
	}

	/*
	 * TODO imprimir productos, ver service ventas
	 * **/
	private String imprimirProductos() {
		String detalle = "";
		for (ProductoVendido p : this.productos) {
			detalle = "\n " + detalle + " " + p.imprimir();				
		}
		return detalle;	
	}
	
	public String imprimir() {
		return "Detalle de la Venta:\n"
				+ "[fechaHora =" + this.fechaHora 
				+ ", cliente = \n" 
				+ this.cliente.imprimir()
				+ "\n , monto = "    
				+ this.monto
				/*+ "\n, productos =" 
				+ this.imprimirProductos()	*/	
				+ "]";	
	}

	public Map<String, Object> toMap() {
		
		var map = new HashMap<String, Object>(
				Map.of("id", id, "monto", monto, "identificadorUnico", identificadorUnico)
		);
		
		System.out.println("*** FIN TO MAP!");
		return map;
	}
}
