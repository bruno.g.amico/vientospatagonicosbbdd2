package ar.unrn.tp.modelo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

@Entity
@Inheritance
@DiscriminatorColumn(name = "tipo_promocion")
public class Promocion {
	@Id
	@GeneratedValue	
	private Long id;
	protected Date desde;
	protected Date hasta;
	protected float porcentajeDesc;
	
	protected Promocion() {
		
	}
	
	public Promocion(Long id, LocalDate desde, LocalDate hasta, float porcentajeDesc) {
		this.validar(desde, hasta, porcentajeDesc);
		this.id = id;
		this.desde = toDate(desde);
		this.hasta = toDate(hasta);
		this.porcentajeDesc = porcentajeDesc;
	}
	
	public Promocion(LocalDate desde, LocalDate hasta, float porcentajeDesc) {
		this.validar(desde, hasta, porcentajeDesc);
		this.desde = toDate(desde);
		this.hasta = toDate(hasta);
		this.porcentajeDesc = porcentajeDesc;
	}

	protected Date toDate(LocalDate fecha) {
		return java.util.Date.from(fecha.atStartOfDay()
			      .atZone(ZoneId.systemDefault())
			      .toInstant());
	}
	
	protected LocalDate toLocalDate(Date fecha) {
		return fecha.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
	}
	
	private void validar(LocalDate desde, LocalDate hasta, float porcentajeDesc) {
		if (porcentajeDesc == 0) {
			throw new RuntimeException("Debe ingresar un Porcentaje de descuento para la promici�n");
		}		
		if (desde == null) {
			throw new RuntimeException("Debe ingresar la fecha de inicio de la promoci�n");
		}
		if (hasta == null) {
			throw new RuntimeException("Debe ingresar la fecha de fin de la promoci�n");
		}		
	}
	
	/*TODO elminar getter and setters de todos las clases luego de las pruebas*/
	public LocalDate getDesde() {
		return this.toLocalDate(desde);
	}
	
	public void setDesde(LocalDate desde) {
		this.desde = this.toDate(desde);
	}	
	
	public LocalDate getHasta() {
		return this.toLocalDate(hasta);
	}

	public void setHasta(LocalDate hasta) {
		this.hasta = this.toDate(hasta);
	}
	
	protected boolean isPromoVigente() {
		LocalDate hoyLD = LocalDate.now();
		Date hoy = Date.from(hoyLD.atStartOfDay(ZoneId.systemDefault()).toInstant());		
		if (this.desde.after(hoy) || this.hasta.before(hoy)) {
			System.out.println("NO Es vigente: " );
			return false;
		}

		return true;
	}
}
