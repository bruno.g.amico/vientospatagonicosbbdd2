package ar.unrn.tp.jpa.servicios;

import java.util.List;

import ar.unrn.tp.modelo.Categoria;
import ar.unrn.tp.modelo.Cliente;
import ar.unrn.tp.modelo.Marca;
import ar.unrn.tp.modelo.Producto;
import ar.unrn.tp.modelo.PromocionDeMarca;
import ar.unrn.tp.modelo.PromocionDePago;
import ar.unrn.tp.modelo.Tarjeta;

public interface SitioDeCompras {

	List<Producto> listarProductos();

	Categoria dameLaCategoriaById(Long idCategoria);

	Producto dameElProductoById(Long idProducto);

	Cliente dameElClienteById(Long idCliente);

	List<Tarjeta> listarTarjetas();

	void hacerPersisnteteCliente(Cliente cliente);

	void hacerPersisnteteProducto(Producto producto);

	void hacerPersistentePromocion(PromocionDePago promo);

	//void hacerPersistentePromocion(PromocionDeCategoria promo);

	void hacerPersistentePromocion(PromocionDeMarca promo);

	Marca dameLaMarcaByDescripcion(String marcaProducto);

	//List<PromocionDeCategoria> listarDescuentos();

}
