package ar.unrn.tp.jpa.servicios;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ar.unrn.tp.api.ProductoService;
import ar.unrn.tp.modelo.Categoria;
import ar.unrn.tp.modelo.Cliente;
import ar.unrn.tp.modelo.Producto;
import ar.unrn.tp.modelo.Marca;

public class DefaultProductoService implements ProductoService {

	//private static final String persistenceUnit = persistenceUnit;
	private static final String persistenceUnit = "jpa-mysql";
	
	public DefaultProductoService() {}

	@Override
	public void crearProducto(String codigo, String descripcion, float precio, Long idCategoria, Long idMarca) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			// TODO
			// implementar una interfaz para el sitio de compras
			// crear este metodo dameLaCategoriaById(idCategoria) 
			
			Categoria categoria = em.find(Categoria.class, idCategoria);
			if (categoria == null) {
				//por ahora solo muestro algo
				System.out.println("\n La categoria" + idCategoria + " no existe [Crear Producto]");
				throw new RuntimeException("La categoria no existe [Crear Producto]");
			}
			
			Marca marca = em.find(Marca.class, idMarca);
			if (marca == null) {
				//por ahora solo muestro algo
				System.out.println("\n La marca " + idMarca + " no existe [Crear Producto]");
				throw new RuntimeException("La marca no existe [Crear Producto]");
			}
			
			Producto producto = new Producto(codigo, descripcion, precio, categoria, marca);
			em.persist(producto);			
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}		
	}

	@Override
	public void modificarProducto(Long idProducto, String descripcion, float precio) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			// TODO
			// implementar una interfaz para el sitio de compras
			// crear este metodo dameElProductoById(idProducto) 
			//new producto...
			Producto producto = em.find(Producto.class, idProducto);
			if (producto != null) {
				producto.setDescripcion(descripcion);
				producto.setPrecio(precio);
			}else {
				//por ahora solo muestro algo
				System.out.println("\n Producto inexistente [Actualizar producto]");
				//throw new RuntimeException("Producto inexistente [Actualizar producto]");
			}
			
			//merge, version
			em.persist(producto);
			Producto updProducto = em.merge(producto);
			em.persist(updProducto);
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}				
	}
	
	/**
	 * 
	 * TEST para tp5
	 * 
	 * */
	public void modificarProducto(Long idProducto, String descripcion, String codigo, float precio, Long version) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			//new producto
			//TODO no setea marca y categoria
			Producto productoActualizado = new Producto(idProducto, descripcion, codigo, precio, version);
			em.merge(productoActualizado);
			
			tx.commit();
			
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}				
	}	

	@Override
	public List<Producto> listarProductos() {

		List<Producto> productos = new ArrayList<>();

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			TypedQuery<Producto> q =
					em.createQuery("select p from Producto p", Producto.class);
			
			productos = q.getResultList();	

			//tx.commit();
		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	

		return productos;

	}

	/*
	 * Lo implemento para el tp5
	 * */
	public Producto traerProducto(Long id) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		Producto producto = null;
		try {
			tx.begin();
			
			producto = em.find(Producto.class, id);
			if (producto == null) {
				throw new RuntimeException("Producto inexistente [traer Producto]");
			}
			
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
		return producto;		
	}		

}
