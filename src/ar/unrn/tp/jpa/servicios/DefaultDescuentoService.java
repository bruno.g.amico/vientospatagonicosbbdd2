package ar.unrn.tp.jpa.servicios;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ar.unrn.tp.api.DescuentoService;
import ar.unrn.tp.modelo.Marca;
import ar.unrn.tp.modelo.PromocionDeMarca;
import ar.unrn.tp.modelo.PromocionDePago;

public class DefaultDescuentoService implements DescuentoService {

	//private static final String persistenceUnit = persistenceUnit;
	private static final String persistenceUnit = "jpa-mysql";
	
	public DefaultDescuentoService() {
	}

	@Override
	// validar que las fechas no se superpongan
	public void crearDescuentoSobreTotal(String marcaTarjeta, LocalDate fechaDesde,	LocalDate fechaHasta, float porcentaje) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			/* tener en cuenta que por el momento dejo aqui la validacion de la 
			 * superposicion de fechas. Sin embargo ya se realiza en la implementacion de la tienda
			 * en el metodo agregarPromocionDePago()
			 * 
			 * */
			
			PromocionDePago promo = new PromocionDePago(fechaDesde, fechaHasta, porcentaje, marcaTarjeta);
			em.persist(promo);
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}				
	}

	@Override
	public void crearDescuento(String marcaProducto, LocalDate fechaDesde, LocalDate fechaHasta, float porcentaje) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();

			// TODO
			// implementar una interfaz para el sitio de compras
			// crear este metodo dameLaMarcaByDescripcion(marcaProducto) 
			TypedQuery<Marca> query =
					em.createQuery("select m from Marca m where m.descripcion = :pMarca", Marca.class);
			query.setParameter("pMarca", marcaProducto);
			
	        List<Marca> resultList = query.getResultList();
	        Marca marca = null;
	        if (!resultList.isEmpty()) {
	        	//tengo la marca, deberia ser unica
	        	//recupero asi para evitar la exception de getSingleResult
	        	marca = resultList.get(0);
	        }else {
	        	throw new RuntimeException("La marca no existe");
	        }
        	PromocionDeMarca promo = new PromocionDeMarca(fechaDesde, fechaHasta, porcentaje, marca);
			em.persist(promo);
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
	}
	
	public List listarDescuentosDePago() {

		List<PromocionDePago> listado = new ArrayList<>();

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			TypedQuery<PromocionDePago> q =
					em.createQuery("select pp from PromocionDePago pp", PromocionDePago.class);
			
			listado = q.getResultList();
			//listado.toString();

			//tx.commit();
		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	

		return listado;

	}	
	

	public List listarDescuentosDeMarca() {

		List<PromocionDeMarca> listado = new ArrayList<>();

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			TypedQuery<PromocionDeMarca> q =
					em.createQuery("select d from PromocionDeMarca d", PromocionDeMarca.class);
			
			listado = q.getResultList();	

			//tx.commit();
		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	

		return listado;

	}		

}
