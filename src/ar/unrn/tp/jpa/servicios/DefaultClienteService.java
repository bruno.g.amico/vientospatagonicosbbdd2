package ar.unrn.tp.jpa.servicios;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ar.unrn.tp.api.ClienteService;
import ar.unrn.tp.modelo.Cliente;
import ar.unrn.tp.modelo.Tarjeta;

public class DefaultClienteService implements ClienteService {
	
	//private static final String persistenceUnit = "jpa-objectdb";
	private static final String persistenceUnit = "jpa-mysql";
	
	public DefaultClienteService() {
	}
	
	@Override
	public void crearCliente(String nombre, String apellido, String dni, String email) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			/* check if dni exist */
			TypedQuery<Cliente> query =
					em.createQuery("select c from Cliente c	where c.dni = :pDni", Cliente.class);
			query.setParameter("pDni", dni);
			
	        List resultList = query.getResultList();
	        
	        if (!resultList.isEmpty()) {
	        	throw new RuntimeException("Ya existe un cliente con el mismo DNI");
	        }else {
				Cliente c = new Cliente(nombre, apellido, dni, email);
				em.persist(c);	        	
	        }
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
	}

	@Override
	public void modificarCliente(Long idCliente, String nombre) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			Cliente c = em.find(Cliente.class, idCliente);
			if (c != null) {
				c.setNombres(nombre);	
			}else {
				//por ahora solo muestro algo
				System.out.println("\n Cliente inexistente [Actualizar Cliente]");
				//throw new RuntimeException("Cliente inexistente [Actualizar Cliente]");
			}
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}		
	}

	@Override
	public void agregarTarjeta(Long idCliente, String nro, String marca) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			Cliente c = em.find(Cliente.class, idCliente);
			if (c != null) {
				Tarjeta tarjeta = new Tarjeta(nro, marca);
				// asociar la tarjeta al cliente
				c.agregarTarjeta(tarjeta);
			}else {
				//por ahora solo muestro algo
				System.out.println("\n Cliente inexistente [Agregar Tarjeta]");
				throw new RuntimeException("Cliente inexistente [Agregar Tarjeta]");
			}			
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	
	}

	@Override
	public List<Tarjeta> listarTarjetas(Long idCliente) {

		List<Tarjeta> tarjetas =  new ArrayList<>();
		List<Tarjeta> tarjetasList =  new ArrayList<>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
			TypedQuery<Cliente> q =
					em.createQuery("select c from Cliente c where c.id = :idCliente", Cliente.class);
					q.setParameter("idCliente", idCliente);
			
			//tarjetas = q.getResultList();
			Cliente cliente = q.getSingleResult();			
			tarjetas = cliente.dameLasTarjeta();
			
			for (Tarjeta tar : tarjetas) {
				tarjetasList.add(tar);
			}

			//tx.commit();
		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}			
		
		
		return tarjetasList;
	}
	

	public Cliente traerCliente(Long idCliente) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		Cliente cliente = null;
		try {
			tx.begin();
			
			cliente = em.find(Cliente.class, idCliente);
			if (cliente == null) {
				throw new RuntimeException("Cliente inexistente [traer Cliente]");
			}
			
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
		return cliente;		
	}	
}
