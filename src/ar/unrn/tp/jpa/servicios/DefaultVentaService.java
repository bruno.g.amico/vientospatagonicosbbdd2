package ar.unrn.tp.jpa.servicios;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ar.unrn.tp.api.VentaService;
import ar.unrn.tp.modelo.Carrito;
import ar.unrn.tp.modelo.Cliente;
import ar.unrn.tp.modelo.NextNumber;
import ar.unrn.tp.modelo.Producto;
import ar.unrn.tp.modelo.PromocionDeMarca;
import ar.unrn.tp.modelo.PromocionDePago;
import ar.unrn.tp.modelo.Tarjeta;
import ar.unrn.tp.modelo.Venta;

public class DefaultVentaService implements VentaService {

	//private static final String persistenceUnit = persistenceUnit;
	private static final String persistenceUnit = "jpa-mysql";

	// Crea una venta. El monto se calcula aplicando los descuentos a la fecha
	// validaciones:
	// - debe ser un cliente existente
	// - la lista de productos no debe estar vac�a
	// - La tarjeta debe pertenecer al cliente
	@Override
	public void realizarVenta(Long idCliente, List productos, Long idTarjeta) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
	        if (productos.isEmpty()) {
	        	throw new RuntimeException("No hay productos para la venta!");  	
	        }
	        
			Cliente cliente = null;
			// recuperar client para la venta
			TypedQuery<Cliente> query =
					em.createQuery("select c from Cliente c	where c.id = :idCliente", Cliente.class);
			query.setParameter("idCliente", idCliente);
			
	        List<Cliente> resultList = query.getResultList();
	        if (!resultList.isEmpty()) {
	        	cliente = resultList.get(0);
	        }else {
	        	throw new RuntimeException("El cliente no existe");        	
	        }			

			// recuperar la tarjeta del cliente por idTarjeta
			Tarjeta tarjetaCliente = cliente.dameTarjetaById(idTarjeta);
			if (tarjetaCliente == null) {
				throw new RuntimeException("La tarjeta no pertenece al cliente");   
			}
			
			//promociones
			// Colleciones de promociones por marca
			TypedQuery<PromocionDeMarca> queryPromoMarca =
					em.createQuery("select pm from PromocionDeMarca pm", PromocionDeMarca.class);
	        List<PromocionDeMarca> promoMarcas = queryPromoMarca.getResultList();
	        
			// Colleciones de promociones de pago
			// Una promo para el pago con tarjeta
			TypedQuery<PromocionDePago> queryPromoPago =
					em.createQuery("select pp from PromocionDePago pp", PromocionDePago.class);
			List<PromocionDePago> promoPagos = queryPromoPago.getResultList();	        
	        
			//obtener listado de productos		
		    javax.persistence.Query qry = em.createQuery("select a from Producto a where a.id in (:lp)");
		    qry.setParameter("lp", productos);
		    List<Producto> productosList = qry.getResultList();
		    
		    //recuperar identificado unico
		    //TENER EN CUENTA que bloqueo aqui, y hasta que no finalice la venta no libero el lock
		    String identificadorUnico = "";
		    //hard-codeo el anio como test
		    LocalDate currentDate = LocalDate.now();
            int anioActual = currentDate.getYear();
            
            System.out.println("*** GENERANDO IDENTIFICADOR UNICO " + anioActual);
            
		    //int anioActual += 1;
			TypedQuery<NextNumber> qProximoNumero =
					em.createQuery("from NextNumber where anio = :anioActual", NextNumber.class);
			qProximoNumero.setParameter("anioActual", anioActual);
			qProximoNumero.setLockMode(LockModeType.PESSIMISTIC_WRITE);
	        NextNumber proximoNumero = qProximoNumero.getSingleResult();
	        
	        int proximo = proximoNumero.recuperarSiguiente();
	        identificadorUnico = Integer.toString(proximo) + "-" + Integer.toString(anioActual);
		    
			//float total = this.calcularMonto(productos, nroTarjeta);
			//instancia del carrito sin la tarjeta de pago
			Carrito carrito = new Carrito(productosList, promoMarcas, promoPagos);
			Venta venta = carrito.realizarVenta(cliente, tarjetaCliente, identificadorUnico);
			
			em.persist(venta);
			
			//guardo la venta en la cache redis
			//guardo aca pq no me libera el lock y no tengo id de venta
			//TODO analizar estrategia, al hacer lpush y lpop desordeno la cache...
			// se hace un lpush o rpush y un ltrim, no usar lpop
			CacheService cache = new CacheService();
			cache.guardarVentaEnLista(venta);
			System.out.println("*** Guarde en cache!!! la venta: " + venta.getIdentificadorUnico());
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
	}

	public float calcularMonto(List productos, String nroTarjeta) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
	
			//promociones
			// Colleciones de promociones por marca
			TypedQuery<PromocionDeMarca> query =
					em.createQuery("select pm from PromocionDeMarca pm", PromocionDeMarca.class);
			
	        List<PromocionDeMarca> promoMarcas = query.getResultList();

			// Colleciones de promociones por marca
			// Una promo para el pago con tarjeta
			TypedQuery<PromocionDePago> queryPromoPago =
					em.createQuery("select pp from PromocionDePago pp", PromocionDePago.class);
			
			List<PromocionDePago> promoPagos = queryPromoPago.getResultList();

			//tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
			
		return 0;
	}

	@Override
	public float calcularMonto(List productos, Long idTarjeta) {

		float total = 0;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();
			
	        if (productos.isEmpty()) {
	        	throw new RuntimeException("No hay productos para la venta!");  	
	        }
	        
			Cliente cliente = null;
			Tarjeta tarjeta = null;
			// recuperar client para la venta
			
			TypedQuery<Tarjeta> query = em.createQuery(
					"select t from Tarjeta t where t.id = :idTarjeta", Tarjeta.class);
			query.setParameter("idTarjeta", idTarjeta);	
			
	        List<Tarjeta> resultList = query.getResultList();
	        if (!resultList.isEmpty()) {
	        	tarjeta = resultList.get(0);
	        }else {
	        	throw new RuntimeException("No existe la tarjeta indicada");        	
	        }
			
			//promociones
			// Colleciones de promociones por marca
			TypedQuery<PromocionDeMarca> queryPromoMarca =
					em.createQuery("select pm from PromocionDeMarca pm", PromocionDeMarca.class);
	        List<PromocionDeMarca> promoMarcas = queryPromoMarca.getResultList();
	        
			// Colleciones de promociones de pago
			// Una promo para el pago con tarjeta
			TypedQuery<PromocionDePago> queryPromoPago =
					em.createQuery("select pp from PromocionDePago pp", PromocionDePago.class);
			List<PromocionDePago> promoPagos = queryPromoPago.getResultList();	        
	        
			//obtener listado de productos		
		    javax.persistence.Query qry = em.createQuery("select a from Producto a where a.id in (:lp)");
		    qry.setParameter("lp", productos);
		    List<Producto> productosList = qry.getResultList();	
		    
			//float total = this.calcularMonto(productos, nroTarjeta);
			//instancia del carrito sin la tarjeta de pago
			Carrito carrito = new Carrito(productosList, promoMarcas, promoPagos);
			total = carrito.calcularMontoTotal(tarjeta);
			
			//tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}
		
		return total;
		
	}

	@Override
	public List ventas() {
		
		List<Venta> ventas = new ArrayList<>();
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();

			TypedQuery<Venta> q =
					em.createQuery("select p from Venta p", Venta.class);
	
			ventas = q.getResultList();

		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	
		return ventas;
	}


	private List ultimasTresVentas(Long idCliente) {
		
		List<Venta> ventas = new ArrayList<>();
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		try {
			tx.begin();

			TypedQuery<Venta> q =
				em.createQuery("SELECT p FROM Venta p WHERE cliente_id = :id ORDER BY p.id DESC", Venta.class)
				.setParameter("id", idCliente);
				q.setMaxResults(3);
				
			ventas = q.getResultList();		

		} catch (NoResultException nre) {
			tx.rollback();
			//si obtengo un resultado vacio en mi busqueda retorno null empyt list por el momento
			return Collections.emptyList();
		} finally {
			if (em != null && em.isOpen())
				em.close();
			if (emf != null)
				emf.close();
		}	
		return ventas;
	}
	
	public List<String> ultimasVentas(Long idCliente) throws Exception {
		
		List<String> list = new ArrayList<String>();
		CacheService cache = new CacheService();
		list = cache.getListadoDeVentas(idCliente);
		//System.out.println("*** LISTADO ultimas ventas: " + list);
		
		if (list.size() < 3){
			// Hay menos de tres en cache debo recuperar de la base las que faltan
			List<Venta> ventas = this.ultimasTresVentas(idCliente);
		    for (int i = ventas.size() - 1; i >= 0; i--) {
		        Venta venta = ventas.get(i); 
		        cache.guardarVentaEnLista(venta);
		    }			
			list = cache.getListadoDeVentas(idCliente);
		}

		return list;
	}
	
}
