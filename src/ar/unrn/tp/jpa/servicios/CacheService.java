package ar.unrn.tp.jpa.servicios;

import java.util.List;
import com.alibaba.fastjson.JSON;

import ar.unrn.tp.modelo.Venta;
import redis.clients.jedis.Jedis;

public class CacheService{

    Jedis jedis = null;

    public CacheService(){    	
    	this.jedis = new Jedis("localhost", 6379);
    }
    
    public void guardarVentaEnLista(Venta venta) {     
        long max = 3;

        String value = JSON.toJSONString(venta.toMap());
        String key = "ult-vent-" + venta.getCliente().getClaveCliente();

        // cargo en la lista
        jedis.lpush(key, value);
        //jedis.rpush(key, value);

        // me aseguro de mantener solo 3 elementos, los ultimos
        jedis.ltrim(key, 0, max - 1);
        //jedis.ltrim(key, -max, -1);

        System.out.println("*** FIN GUARDAR VENTA EN LISTADO: " + key);       
    }
    
    public List<String> getListadoDeVentas(Long idCliente) {
        long maxResultados  = 3;
        String key = "ult-vent-" + idCliente;
        
        //vacio la lista, test
        //jedis.del(key);
        
        List<String> list = this.jedis.lrange(key, 0, maxResultados  - 1);
        
        return list;
    }

}